package controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import mainApp.Budgeteer;

public class NavBarController extends Controller{
	@FXML
	private ImageView logo;
	
	@FXML
	private void initialize() {
		this.logo.setImage(new Image(Budgeteer.class.getResourceAsStream("../images/icon1.png")));
	}
	
	@FXML
	private void handleLogout() {
		this.mainApp.initRootLayout();
		this.mainApp.showPage("LoginPageView.fxml", new LoginPageController());
	}
	
	@FXML
	private void handleHome() {
		this.mainApp.showPage("HomePageView.fxml", new HomePageController());
	}
	
	@FXML
	private void handleView() {
		System.out.println("Entering view");
		this.mainApp.showPage("TransactionManagementView.fxml", new TransactionManagementController());
	}
	
	@FXML
	private void handleCategories() {
		System.out.println("Entering categories");
		this.mainApp.showPage("CategoryOverviewView.fxml", new CategoryOverviewController());
	}
	
	@FXML
	private void handleProfile() {
		System.out.println("Entering profile");
		this.mainApp.showPage("ProfileView.fxml", new ProfileController());
	}
	
	@FXML
	private void handleStats() {
		this.mainApp.showPage("StatsView.fxml", new StatsController());
	}
}
