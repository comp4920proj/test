package controller;

import javafx.geometry.Pos;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import model.ExpenseCategory;

public class ProgressCell extends TableCell<ExpenseCategory, Double> {
	
	private ProgressBar progress = new ProgressBar();
	
	public ProgressCell(){
		progress.setMaxWidth(Double.MAX_VALUE);
		progress.setMaxHeight(Double.MAX_VALUE);
	}
	
	@Override
	protected void updateItem(Double amount, boolean empty) {
		super.updateItem(amount, empty);
		
		if (!empty && amount >= 1) {
			progress.setProgress(amount);
			progress.setStyle("-fx-accent: red");
			setGraphic(progress);
		} else if (!empty) {
			progress.setProgress(amount);
			progress.setStyle("-fx-accent: green");
			setGraphic(progress);
		}
	}
}
