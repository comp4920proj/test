package controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.function.UnaryOperator;

import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;
import javafx.util.converter.DateTimeStringConverter;

import javafx.event.Event;
import javafx.event.EventHandler;

/*
 *  THIRD PARTY CODE
 *  Adapted from: https://stackoverflow.com/questions/36087968/adding-currency-symbol-to-tableview-but-remove-on-edit-cell
 *  Well-defined purpose: extension of table cell in JavaFX to support DatePicker in tables 
 */


public class DatePickerCell<T> extends TableCell<T, Date> {

    private final TextField textField ;

    private final DateFormat format = DateFormat.getDateInstance();
    private final DateFormat textFieldFormat = DateFormat.getDateInstance();
    private DatePicker datePicker;

    public DatePickerCell() {
        this.textField = new TextField();
        if (datePicker == null) {
        	createDatePicker();
        }
        StringConverter<Date> converter = new DateTimeStringConverter();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        UnaryOperator<Change> filter = (Change change) -> {
            String newText = change.getControlNewText() ;
            if (newText.isEmpty()) {
                return change ;
            }
            try {
                sdf.parse(newText);
                return change ;
            } catch (ParseException exc) {
                return null ;
            }
        };
        TextFormatter<Date> textFormatter = new TextFormatter<Date>(converter);
        textField.setTextFormatter(textFormatter);

        textField.setOnAction(e -> commitEdit(converter.fromString(textField.getText())));
        textField.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            if (e.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });

        setGraphic(textField);
        setContentDisplay(ContentDisplay.TEXT_ONLY);

    }

    @Override
    protected void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        } else if (isEditing()) {
            textField.setText(item.toString());
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
        	setText(format.format(item));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        setGraphic(datePicker);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        setText(sdf.format(getItem()));
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        textField.requestFocus();
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(format.format(getItem()));
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void commitEdit(Date newValue) {
        super.commitEdit(newValue);
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
    
    private void createDatePicker() {
        this.datePicker = new DatePicker();
        datePicker.setPromptText("dd/mm/yyyy");
        datePicker.setEditable(true);

        datePicker.setOnAction(new EventHandler() {
            public void handle(Event t) {
                LocalDate date = datePicker.getValue();
                int index = getIndex();

                SimpleDateFormat smp = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_MONTH, date.getDayOfMonth());
                cal.set(Calendar.MONTH, date.getMonthValue() - 1);
                cal.set(Calendar.YEAR, date.getYear());

                setText(smp.format(cal.getTime()));
                commitEdit(cal.getTime());

            }
        });

        setAlignment(Pos.CENTER);
    }
}