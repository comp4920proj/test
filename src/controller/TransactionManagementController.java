
package controller;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import dao.AccountCategoryDAO;
import dao.AccountDAO;
import dao.TransactionDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.BooleanStringConverter;
import javafx.util.converter.DateStringConverter;
import javafx.util.converter.DateTimeStringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.LocalDateStringConverter;
import mainApp.Budgeteer;
import model.Budget;
import model.Category;
import model.ExpenseCategory;
import model.Transaction;

public class TransactionManagementController extends Controller {
	@FXML
	private TextField descriptionField;
	@FXML
	private Button searchButton;
	@FXML
	private DatePicker fromDate;
	@FXML
	private DatePicker toDate;
	@FXML
	private MenuItem monthlyMenuItem;
	@FXML
	private MenuItem yearlyMenuItem;
	
	@FXML
	private TextField addAmount;
	@FXML
	private DatePicker addDate;
	@FXML
	private TextField addDescription;
	@FXML
	private CheckBox addFavourite;
	@FXML
	private ChoiceBox<String> addCategoryChoice;
	@FXML
	private ChoiceBox<String> transactionType;
	@FXML
	private ChoiceBox<String> repeatChoice;
	@FXML
	private DatePicker repeatDate;
	
	@FXML
	private TableView<Transaction> transactionTable;
	@FXML
    private TableColumn<Transaction, java.util.Date> dateColumn;
	@FXML
    private TableColumn<Transaction, Double> amountColumn;
	@FXML
    private TableColumn<Transaction, String> categoryColumn;
	@FXML
    private TableColumn<Transaction, String> descriptionColumn;
	@FXML
    private TableColumn<Transaction, String> favouriteColumn;
	@FXML
	private TableColumn<Transaction, String> typeColumn;
	@FXML
	private Button deleteButton;
	@FXML
	private Button clearButton;
	@FXML
	private CheckBox showFuture;
	
	
	@FXML
	private ChoiceBox<String> categoryChoice;

	private AccountDAO accountDAO;
	
	private AccountCategoryDAO accountCategoryDAO;
	
	private TransactionDAO transactionDAO;
	
	
	private ObservableList<String> transactionTypeStrings = FXCollections.observableArrayList(
		    "Expense", "Income");
	
	private ObservableList<String> favouriteStrings = FXCollections.observableArrayList(
		    "Yes", "No");
	
	private ObservableList<String> repeatStrings = FXCollections.observableArrayList(
		    "Repeat?", "Daily", "Weekly", "Fortnightly", "Monthly", "Yearly");
	
	private ObservableList<Transaction> allTransactions = FXCollections.observableArrayList();
	private ObservableList<Category> incomeCategoriesObjects = FXCollections.observableArrayList();
	private ObservableList<ExpenseCategory> expenseCategoriesObjects = FXCollections.observableArrayList();
	private ObservableList<String> everyCategory = FXCollections.observableArrayList();
	private ObservableList<String> everyCategoryChoice = FXCollections.observableArrayList();
	  
	private LocalDate start;
	private LocalDate end; 
	private String description = "";
	private String category = "Categories...";
	
	HashMap<String, Category> categoryMap = new HashMap<String, Category>();
	
	
	public TransactionManagementController() {
		this.accountDAO = new AccountDAO();
		this.transactionDAO = new TransactionDAO();
	}
	
	public void initialize() {
		//change the period to weekly after testing is done
		List<Transaction> transactions = this.mainApp.getTransactions();
		for (Transaction t : transactions) {
			allTransactions.add(t);
		}
		drawGrid(transactions);
		initialiseChoiceBoxes();
	}
	
	private void initialiseChoiceBoxes () {
		expenseCategoriesObjects = this.mainApp.getAllCategories();
		ObservableList<String> allCategories = FXCollections.observableArrayList();
		
		expenseCategoriesObjects.forEach((c) -> {
			allCategories.add(c.getName());
			everyCategory.add(c.getName());
			everyCategoryChoice.add(c.getName());
			categoryMap.put(c.getName(), c);
		});
		allCategories.add(0, "Categories...");
//		everyCategory.add(0, "Categories...");
		
		incomeCategoriesObjects = this.mainApp.getIncomeCategories();
		ObservableList<String> incomeCategories = FXCollections.observableArrayList();
		
		incomeCategoriesObjects.forEach((c) -> {
			incomeCategories.add(c.getName());
			everyCategory.add(c.getName());
			everyCategoryChoice.add(c.getName());
			categoryMap.put(c.getName(), c);
		});
		incomeCategories.add(0, "Categories...");
		everyCategoryChoice.add(0, "Categories...");
		
		addCategoryChoice.setItems(allCategories);
		addCategoryChoice.setValue("Categories...");
		categoryChoice.setItems(everyCategoryChoice);
		categoryChoice.setValue("Categories...");
		transactionType.setItems(transactionTypeStrings);
		transactionType.setValue("Expense");
		repeatChoice.setItems(repeatStrings);
		repeatChoice.setValue("Repeat?");
		
		transactionType.getSelectionModel().selectedIndexProperty().addListener(new
				ChangeListener<Number>() {

				public void changed(ObservableValue ov, Number value, Number newValue) {
					if (newValue.intValue() == 0) {						
						addCategoryChoice.setItems(allCategories);
						addCategoryChoice.setValue("Categories...");
					} else {
						addCategoryChoice.setItems(incomeCategories);
						addCategoryChoice.setValue("Categories...");
					}
				}
				});
	}
	
	
	private void drawGrid (List<Transaction> transactions) {
		dateColumn.setCellValueFactory(new PropertyValueFactory<Transaction, java.util.Date>("date"));
		// probably need to make a custom table cell class for this one
//		dateColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DateStringConverter()));
		dateColumn.setCellFactory(tc -> new DatePickerCell<>());
		dateColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Transaction, java.util.Date>>() {
                	@Override
                	public void handle(CellEditEvent<Transaction, java.util.Date> t) {
                		Transaction transaction = (t.getTableView().getItems().get(t.getTablePosition().getRow()));
                		transaction.setDate(Date.valueOf(t.getNewValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
                		//handle edit here
                		Budget.editDate(transaction, Date.valueOf(t.getNewValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
                	}
                }
            );
		
        amountColumn.setCellValueFactory(new PropertyValueFactory<Transaction, Double>("amount"));
//        amountColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        amountColumn.setCellFactory(tc -> new CurrencyCell<>());
        amountColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Transaction, Double>>() {
                	@Override
                	public void handle(CellEditEvent<Transaction, Double> t) {
                		Transaction transaction = (t.getTableView().getItems().get(t.getTablePosition().getRow()));
                		transaction.setAmount(t.getNewValue());
                		//handle edit here
                		Budget.editAmount(transaction, t.getNewValue());
                		if (transaction.getIsExpenseProperty().getValue()) {
        					if (mainApp.hasReachedLimit((Category) categoryMap.get(transaction.getCategoryName()))) {
        						Alert alert = new Alert(AlertType.WARNING);
        						alert.setTitle("Limit Warning");
        						alert.setHeaderText("You have spent over 80% of your limit for " + category);
        			            alert.showAndWait();
        					}
        				}
                	}
                }
            );
        
        //        categoryColumn.setCellValueFactory(new PropertyValueFactory<Transaction, String>("category"));
        categoryColumn.setCellValueFactory(new PropertyValueFactory<Transaction, String>("categoryName"));
        categoryColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(everyCategory));
//        categoryColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        categoryColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Transaction, String>>() {
                	@Override
                	public void handle(CellEditEvent<Transaction, String> t) {
                		Transaction transaction = (t.getTableView().getItems().get(t.getTablePosition().getRow()));
                		//handle edit here
                		transaction.setCategoryName(t.getNewValue());
                		for (Category c : incomeCategoriesObjects) {
                			if (t.getNewValue().equals(c.getName())) {
                				transaction.setIsExpense(false);
                				break;
                			} else {
                				transaction.setIsExpense(true);
                			}
                		}
                		
                		String c = t.getNewValue();
                		Category category = (Category) categoryMap.get(c);
                		transaction.setCategoryID(category.getCategoryID());
//                		mainApp.editCategory(t.getNewValue(), category.getCategoryID());
                		// check if category in one of the category arrays
                		Budget.editCategory(transaction, category.getCategoryID());
                		updateView();
                	}
                }
            );
        
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<Transaction, String>("description"));
        descriptionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        descriptionColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Transaction, String>>() {
                	@Override
                	public void handle(CellEditEvent<Transaction, String> t) {
                		Transaction transaction = (t.getTableView().getItems().get(t.getTablePosition().getRow()));
                		//handle edit here
                		transaction.setDescription(t.getNewValue());
                		Budget.editDescription(transaction, t.getNewValue());
                	}
                }
            );
        
        favouriteColumn.setCellValueFactory(cellData -> cellData.getValue().getFavouriteString());
        favouriteColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(favouriteStrings));
        favouriteColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<Transaction, String>>() {
                	@Override
                	public void handle(CellEditEvent<Transaction, String> t) {
                		boolean favourite = (t.getNewValue().equals("Yes")) ? true : false;
                		Transaction transaction = (t.getTableView().getItems().get(t.getTablePosition().getRow()));
                		//handle edit here
                		transaction.setFavourite(favourite);
                		Budget.editFavourite(transaction, favourite);
                	}
                }
            ); 
        
        typeColumn.setCellValueFactory(cellData -> cellData.getValue().getTypeString());
        transactionTable.setItems(allTransactions);
	}
	
//	@FXML 
//	private void handleAdd() {
//		Budget.addTransaction(1, newDate.getText(), newAmount.getText(), newCategory.getText(), 
//								newDescription.getText(), newFavourite.getText());
//	}
	
	@FXML
	private void handleViewChange() {
		//transactionGrid.getChildren().clear();
		start = (fromDate != null) ? fromDate.getValue() : null;
		end =  (toDate != null) ? toDate.getValue() : null;
		description = (descriptionField != null) ? descriptionField.getText() : "";
		category = categoryChoice.getValue().toString();
		updateView();
//		handleView();
	}
	
	private void updateView() {
		List<Transaction> transactions;
		transactions = this.mainApp.getSpecificTransactions(start, end, description, category);
			
		allTransactions.clear();
		for (Transaction t : transactions) {
			allTransactions.add(t);
		}
		drawGrid(transactions);
	}
	
	@FXML
	private void handleDelete() {
		Transaction t = transactionTable.getSelectionModel().getSelectedItem();
		if (t == null) {
			return;
		}	
		this.mainApp.deleteTransaction(t);
		allTransactions.remove(t);
		drawGrid(allTransactions);
	}
	
	@FXML
	private void handleClear() {
		descriptionField.clear();
		fromDate.setValue(null);
		toDate.setValue(null);
		categoryChoice.setValue("Categories...");
		handleViewChange();
	}
	
	@FXML
	private void handleAdd() {
		if (addAmount.getText() == "" || addAmount.getText().startsWith("-") || addAmount.getText().equals("0")) {
			// Show the error message.
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Please correct invalid fields");
            alert.setHeaderText("Empty field or negative amount detected");
            alert.setContentText("Enter a non-zero positive amount");
            alert.showAndWait();
            addAmount.clear();
			addDate.setValue(null);
			addCategoryChoice.setValue("Categories...");
			repeatChoice.setValue("Repeat?");
			repeatDate.setValue(null);
			addDescription.clear();			
			addFavourite.setSelected(false);
			return;
		} else if (!repeatChoice.getValue().equals("Repeat?") && repeatDate.getValue() == null) { 
			Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Please correct invalid fields");
            alert.setHeaderText("Empty repeat until field");
            alert.setContentText("Enter a date you want to repeat the transaction until");
            alert.showAndWait();
            return;
		} else if (repeatChoice.getValue().equals("Repeat?") && repeatDate.getValue() != null){
			Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Please correct invalid fields");
            alert.setHeaderText("Empty repeat period field");
            alert.setContentText("Enter a date you want to repeat the transaction until");
            alert.showAndWait();
            return;
		} else if (!repeatChoice.getValue().equals("Repeat?") && repeatDate.getValue() != null) {
			if (addDate.getValue() != null) {
				if (repeatDate.getValue().isBefore(addDate.getValue())) {
					Alert alert = new Alert(AlertType.INFORMATION);
		            alert.setTitle("Please correct invalid fields");
		            alert.setHeaderText("Repeat until date is before the add transaction date");
		            alert.setContentText("Enter a repeat until date that is past the add transaction date");
		            alert.showAndWait();
		            return;
				}
			} else {
				if (repeatDate.getValue().isBefore(LocalDate.now())) {
					Alert alert = new Alert(AlertType.INFORMATION);
		            alert.setTitle("Please correct invalid fields");
		            alert.setHeaderText("No transaction date");
		            alert.setContentText("Enter a transaction date");
		            alert.showAndWait();
		            return;
				}
			}
		} 
		
		
			try {
				String category = null;
				String type = null;
				double newAmount = Double.parseDouble(addAmount.getText());
				java.sql.Date newDate;
				java.sql.Date repeatUntil = null;
				
				Calendar c = Calendar.getInstance();
				
				if (addDate.getValue() != null) {
					c.setTime(Date.valueOf(addDate.getValue()));
					newDate = new java.sql.Date(c.getTimeInMillis());
				} else {
					newDate = new java.sql.Date(c.getTimeInMillis());
				}
				
				if (repeatDate.getValue() != null) {
					c.setTime(Date.valueOf(repeatDate.getValue()));
					repeatUntil = new java.sql.Date(c.getTimeInMillis());
				}
				
				String newDescription = addDescription.getText();
				
				boolean favourite = false;
				if (addFavourite.isSelected()) {
					favourite = true;
				}
				
				if (transactionType.getValue().toString() == "Expense") {
					type = "Expense";
				} else {
					type = "Income";
				}
				
				if (addCategoryChoice.getValue().toString() != "Categories...") {
					category = addCategoryChoice.getValue().toString();
				} else {
					category = (type.equals("Expense") ? "Other Expense" : "Other Income");
				}
				System.out.println("Category is " + category);
				String repeatPeriod = repeatChoice.getValue().toString();
				Category categoryObject = (Category) categoryMap.get(category);
				
				this.mainApp.addTransaction(newAmount, newDate, categoryObject, newDescription, favourite, type, repeatPeriod, repeatUntil);
				updateView();
				if (type == "Expense") {
					if (this.mainApp.hasReachedLimit(categoryObject)) {
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Limit Warning");
						alert.setHeaderText("You have spent over 80% of your limit for " + category);
			            alert.showAndWait();
					}
				}
			} catch (NumberFormatException e) {
				// Show the error message.
	            Alert alert = new Alert(AlertType.INFORMATION);
	            alert.setTitle("Invalid Input");
	            alert.setHeaderText("Please correct invalid fields");
	            alert.setContentText("Amount is not a number");
	            alert.showAndWait();
			} finally {
				addAmount.clear();
				addDate.setValue(null);
				addCategoryChoice.setValue("Categories...");
				repeatChoice.setValue("Repeat?");
				repeatDate.setValue(null);
				addDescription.clear();			
				addFavourite.setSelected(false);
//				handleView();
			}
		
		
	}
}

