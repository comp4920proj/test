package controller;

import dao.TransactionDAO.Period;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import model.Category;
import model.ExpenseCategory;
import util.InputValidator;

import java.util.Optional;

import dao.Budget;

public class CategoryAddController extends Controller {
	private ToggleGroup categoryType;
	@FXML
	private ToggleButton incomeToggleButton;
	@FXML
	private ToggleButton expenseToggleButton;
	@FXML
	private TextField categoryNameTextField;
	@FXML
	private TextField categoryLimitTextField;
	@FXML
	private Label categoryLimitLabel;
	@FXML
	private Label periodLabel;
	@FXML
	private ChoiceBox<Period> periodChoiceBox;

	private Budget budget;
	
	@FXML
	private void initialize() {
		this.categoryType = new ToggleGroup();
		this.budget = new Budget();
		
		this.incomeToggleButton.selectedProperty().addListener(
				(observable, oldValue, newValue) -> changeLabel());
		
		// select income button initially
		this.incomeToggleButton.setSelected(true);
	}
	
	/**
	 * Changes the visibility of the limit text field and label depending on whether
	 * the income or expense button is selected
	 */
	private void changeLabel() {
		if (this.incomeToggleButton.isSelected()) {
			this.categoryLimitLabel.setVisible(false);
			this.categoryLimitTextField.setVisible(false);
			this.periodLabel.setVisible(false);
			this.periodChoiceBox.setVisible(false);
		} else if (this.expenseToggleButton.isSelected()) {
			this.categoryLimitLabel.setVisible(true);
			this.categoryLimitTextField.setVisible(true);
			this.categoryLimitTextField.setText("0.00");
			this.periodLabel.setVisible(true);
			this.periodChoiceBox.setVisible(true);
			
			ObservableList<Period> periodList = FXCollections.observableArrayList(Period.DAILY, Period.WEEKLY, Period.MONTHLY, Period.YEARLY); 
			this.periodChoiceBox.setItems(periodList);
			
			// default choice is weekly
			this.periodChoiceBox.getSelectionModel().select(periodList.indexOf(Period.WEEKLY));
		}
	}
	
	@FXML
	private void handleAddButton() {
		String categoryName = this.categoryNameTextField.getText().trim();
		String limit = this.categoryLimitTextField.getText().trim();
		int accountID = this.mainApp.getLoggedInAs().getAccountID();

		try {
			if (this.incomeToggleButton.isSelected()) {
				if (InputValidator.isNotEmptyString(categoryName)) {
					boolean validInput = true;
					
					if (this.isSameName(categoryName, accountID, false)) {	
						Optional<ButtonType> b = this.createConfirmationDialog("Same Name", "Category With Same Name Exists", 
								"Create this category?");
						
						if (b.get() != ButtonType.OK) {
							validInput = false;
						}
					}
					
					if (validInput) {
						this.budget.addIncomeCategory(categoryName, accountID);
					
						// display overview page again after adding category
						this.mainApp.showPage("CategoryOverviewView.fxml", new CategoryOverviewController());
					}
				} else {
					this.createErrorDialog("Empty Field", "Name Cannot be Empty", "Category name field is empty");
				}
			} else if (this.expenseToggleButton.isSelected()) {
				boolean validInput = true;
				
				// validate category name text field
				if (!InputValidator.isNotEmptyString(categoryName)) {
					validInput = false;
					this.createErrorDialog("Empty Field", "Name Cannot be Empty", "Category name field is empty");
				}
				
				// validate limit text field
				if (!InputValidator.isPrice(limit)) {
					validInput = false;
					this.createErrorDialog("Invalid Input", "Invalid Limit", "Limit cannot be empty and must contain at least 1 digit");
				}
				
				if (this.isSameName(categoryName, accountID, true)) {	
					Optional<ButtonType> b = this.createConfirmationDialog("Same Name", "Category With Same Name Exists", 
							"Create this category?");
					
					if (b.get() != ButtonType.OK) {
						validInput = false;
					}
				}
				
				if (validInput) {
					double maxAmount = Double.parseDouble(limit);
					
					this.budget.addExpenseCategory(categoryName, maxAmount, accountID, periodChoiceBox.getValue());
					
					// display overview page again after adding category
					this.mainApp.showPage("CategoryOverviewView.fxml", new CategoryOverviewController());

				}
			}
		} catch (Exception e) {
			this.createDBErrorDialog(e.getMessage());
		}
	}
	
	@FXML
	private void handleCancelButton() {
		this.mainApp.showCategoryOverviewPage();
	}
	
	private boolean isSameName(String categoryName, int accountID, boolean isExpenseCategory) {
		boolean isSame = false;
		ObservableList<Category> list = budget.searchCategoriesExact(categoryName, accountID);
		
		if (list.size() > 0) {
			for (Category c: list) {
				try {
					boolean b = budget.isExpenseCategory(c.getCategoryID());
					if (isExpenseCategory && b) {
						isSame = true;
					} else if (!isExpenseCategory && !b) {
						isSame = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return isSame;
	}
}
