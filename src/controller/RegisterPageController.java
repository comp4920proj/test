package controller;

import dao.AccountDAO;
import dao.CategoryDAO;
import dao.ExpenseCategoryDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.Account;
import util.InputGenerator;

public class RegisterPageController extends Controller {
	@FXML
	private TextField usernameTextField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private PasswordField confirmPasswordField;
	@FXML
	private Label statusLabel;
	@FXML
	private Button registerButton;
	private AccountDAO accountDAO;
	
    @FXML
    private void initialize () {      	
    	this.accountDAO = new AccountDAO();
    }
	
	@FXML
	public void handleRegisterButton() {
		if (this.fieldsNotEmpty()) {
			// validate input
			if (!this.isUniqueUsername()) {
				this.createErrorDialog("Invalid Username", "Username has been taken", "Use another username");
			} else if (!this.isSufficientLength()) {
				this.createErrorDialog("Invalid Password", "Password too weak", "Password must be at least 4 characters long");
			} else if (!this.passwordFieldsMatch()) {
				this.createErrorDialog("Invalid Password", "Passwords don't match", "Password and confirm password fields must match");
			} else {
				// TODO: input validation in Account class
				Account account = new Account();
				account.setUsername(this.usernameTextField.getText());
				account.setPassword(this.passwordField.getText());
				
				int accountID = this.accountDAO.addObjectAndGetID(account);
				
				// provide account with some categories to start
				InputGenerator.insertDefaultCategories(accountID, this);
							
				// Re-direct to login page after registration
				this.getMainApp().showPage("LoginPageView.fxml", new LoginPageController());
			}
		} else {
			this.createErrorDialog("Empty Fields", "Fields cannot be empty", "Fill in all fields");
		}
	}
	
	@FXML
	public void handleBackButton() {
		this.getMainApp().showPage("LoginPageView.fxml", new LoginPageController());
	}
	
	/**
	 * Checks if password and confirm password fields match
	 * @return true if they match
	 */
	private boolean passwordFieldsMatch() {
		return this.passwordField.getText().equals(this.confirmPasswordField.getText());
	}
	
	/**
	 * Checks if fields are not empty
	 * @return true if there is text in all fields
	 */
	private boolean fieldsNotEmpty() {
		return !this.usernameTextField.getText().isEmpty() &&
				!this.passwordField.getText().isEmpty() &&
				!this.confirmPasswordField.getText().isEmpty();
	}
	
	/**
	 * Checks if username has already been taken
	 * @return true is username has not been taken
	 */
	private boolean isUniqueUsername() {
		Account account = accountDAO.getObject(this.usernameTextField.getText());
		
		if (account == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if password has sufficient length
	 * @return true if it meets the conditions
	 */
	private boolean isSufficientLength() {
		final int length = 3;
		return this.passwordField.getText().length() > length;
	}
}
