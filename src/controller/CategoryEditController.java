package controller;

import java.text.DecimalFormat;

import dao.Budget;
import dao.TransactionDAO.Period;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Category;
import model.Limit;
import util.InputValidator;

public class CategoryEditController extends Controller {
	@FXML
	private TextField categoryNameTextField;
	@FXML
	private TextField categoryLimitTextField;
	@FXML
	private Label categoryLimitLabel;
	@FXML
	private Label categoryPeriodLabel;
	@FXML
	private Label nameLabel;
	@FXML
	private Button editButton;
	@FXML
	private ChoiceBox<Period> periodChoiceBox;
	
	private boolean isExpense;
	private Category category;
	private Limit limit;
	
	private Budget budget;
	
	public CategoryEditController(Category category, boolean isExpense) {
		this.category = category;
		this.isExpense = isExpense;
		this.budget = new Budget();
		
		if (isExpense) {
			this.limit = budget.getLimit(this.category.getCategoryID());
		} else {
			this.limit = null;
		}
	}
	
	@FXML
	private void initialize() {
		// constructor is always called before initialize
		// isExpense will be initialize at this point
		if (!this.isExpense) {
			// hide all expense related fields
			this.categoryLimitLabel.setVisible(false);
			this.categoryPeriodLabel.setVisible(false);
			this.categoryLimitTextField.setVisible(false);
			this.periodChoiceBox.setVisible(false);
		} else {
			DecimalFormat df = new DecimalFormat("0.00"); 
			this.categoryLimitTextField.setText(df.format(this.limit.getMaxAmount()));
			
			ObservableList<Period> periodList = FXCollections.observableArrayList(Period.DAILY, Period.WEEKLY, Period.MONTHLY, Period.YEARLY); 
			this.periodChoiceBox.setItems(periodList);
			
			// set default choice to category limit's period
			int defaultSelection = periodList.indexOf(this.limit.getPeriod());
			this.periodChoiceBox.getSelectionModel().select(defaultSelection);
		}

		// set text here as it's required for the edit button
		this.categoryNameTextField.setText(this.category.getName());
		
		// remove the textField is category isn't editable
		if (!category.getIsDeletable()) {
			this.categoryNameTextField.setVisible(false);
			this.nameLabel.setText(this.category.getName());
		} else {
			this.nameLabel.setVisible(false);
		}
		
		// remove edit button if income category and not editable
		if (!this.isExpense && !category.getIsDeletable()) {
			this.editButton.setVisible(false);
		} else {
			this.editButton.setVisible(true);
		}
	}
	
	@FXML
	private void handleEditButton() {
		String categoryName = this.categoryNameTextField.getText().trim();
		boolean validInput = true;

		// check if category name is not empty
		if (InputValidator.isNotEmptyString(categoryName)) {
			this.category.setName(categoryName);
		} else {
			this.createErrorDialog("Empty Field", "Name Cannot be Empty", "Category name field is empty");
			validInput = false;
		}
		
		if (isExpense) {
			String limitText = this.categoryLimitTextField.getText().trim();
			
			// set max amount
			if (InputValidator.isPrice(limitText)) {
				this.limit.setMaxAmount(Double.parseDouble(limitText));
				
				// set value of period
				this.limit.setPeriod(periodChoiceBox.getValue());
			} else {
				this.createErrorDialog("Invalid Input", "Invalid Limit", "Limit must be positive amount and have at most 2 decimal places.");
				validInput = false;
			}
		}
		
		if (validInput) {			
			this.budget.updateCategory(category);
			
			// only update limit if it's an expense category getting updated
			if (isExpense) {
				this.budget.updateLimit(limit);
			}
			
			this.mainApp.showCategoryOverviewPage();
		}

	}
	
	@FXML
	private void handleCancelButton() {
		this.mainApp.showCategoryOverviewPage();
	}
}
