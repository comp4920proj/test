package controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.stream.Collectors;

import dao.Balance;
import dao.Budget;
import dao.TransactionDAO;
import dao.TransactionDAO.Period;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import model.Category;
import model.ExpenseCategory;
import model.Row;
import model.Transaction;
import util.CalFormatter;
import util.CalendarPair;
import dao.CategoryDAO;
import dao.ExpenseCategoryDAO;

public class StatsController extends Controller {
	@FXML
	private Button previousButton;
	@FXML
	private Button nextButton;
	@FXML
	private ChoiceBox<Period> periodChoiceBox;

	@FXML
	private TabPane statsTabPane;
	@FXML
	private Tab balanceTab;
	@FXML
	private LineChart<String, Double> lineChart;
	@FXML
	private Label balanceLabel;
	
	@FXML
	private Tab expensesTab;
	@FXML
	private PieChart expensesPieChart;

	@FXML
	private Tab incomeTab;
	@FXML
	private PieChart incomePieChart;

	@FXML
	private Tab expensesByCategoryTab;
	@FXML
	private TableView<Row> expensesByCategoryTable;
	@FXML
	private TableColumn<Row, String> expenseCategoryColumn;
	@FXML
	private TableColumn<Row, String> expenseAmountColumn;
	@FXML
	private Label expenseTableTitle;
	
	@FXML
	private TableView<Transaction> transactionTable;
	@FXML
	private TableColumn<Transaction, String> transactionDateColumn;
	@FXML
	private TableColumn<Transaction, String> transactionAmountColumn;
	@FXML
	private TableColumn<Transaction, String> transactionDescriptionColumn;
	@FXML
	private Button moreInformationButton;
	@FXML
	private GridPane buttonGridPane;
	
    private SimpleDateFormat fmt;      	
	private Calendar calendar;
	private Balance balanceCalculator;
	private Budget budget;
	private TransactionDAO tDAO;
	private ExpenseCategoryDAO ecDAO;
	private CategoryDAO cDAO;
	private ObservableList<Row> expenseTableData;
	private ObservableList<PieChart.Data> expensePieChartData;
	private ObservableList<PieChart.Data> incomePieChartData;
	private ObservableList<Transaction> transactionList;
	private DoubleBinding expensePieChartTotal;
	private DoubleBinding incomePieChartTotal;

	@FXML
	private void initialize() {
		ObservableList<Period> periodList = FXCollections.observableArrayList(Period.DAILY, Period.WEEKLY, Period.MONTHLY, Period.YEARLY); 
			
		this.periodChoiceBox.setItems(periodList);
		this.periodChoiceBox.getSelectionModel().select(periodList.indexOf(Period.WEEKLY));

		this.balanceTab.selectedProperty().addListener(
				(observable, oldValue, newValue) -> this.toggleBalanceLabel(newValue));
		
		this.balanceLabel.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
		this.balanceLabel.setStyle("-fx-font-size: 11; -fx-font-weight: bold;");
		this.balanceLabel.setText("Balance: ");
		
		this.lineChart.setLegendVisible(false);
		// hide tick marks from chart
    	this.lineChart.setCreateSymbols(false);
		this.updateData(this.periodChoiceBox.getSelectionModel().getSelectedItem(), 
				this.statsTabPane.getSelectionModel().getSelectedItem());
		
		this.periodChoiceBox.getSelectionModel().selectedItemProperty().addListener(
    			(observable, oldValue, newValue) -> this.updateData(
    					newValue, this.statsTabPane.getSelectionModel().getSelectedItem()));
		
		this.statsTabPane.getSelectionModel().selectedItemProperty().addListener(
    			(observable, oldValue, newValue) -> this.updateData(
    					this.periodChoiceBox.getSelectionModel().getSelectedItem(), newValue));	
		
		this.expensesByCategoryTab.selectedProperty().addListener(
				(observable, oldValue, newValue) -> this.toggleControls(newValue));
		
		this.expensePieChartData = FXCollections.observableArrayList();
		this.expensePieChartTotal = Bindings.createDoubleBinding(() ->
		this.expensePieChartData.stream().collect(Collectors.summingDouble(PieChart.Data::getPieValue)), expensePieChartData);
		
		this.incomePieChartData = FXCollections.observableArrayList();
		this.incomePieChartTotal = Bindings.createDoubleBinding(() ->
		this.incomePieChartData.stream().collect(Collectors.summingDouble(PieChart.Data::getPieValue)), incomePieChartData);
		
		this.expensesByCategoryTable.setPlaceholder(new Label("There was no spending in this period."));
		this.expenseTableData = FXCollections.observableArrayList();
		this.expenseCategoryColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
		this.expenseAmountColumn.setCellValueFactory(cellData -> cellData.getValue().getAmount());
		this.expensesByCategoryTable.setItems(expenseTableData);
		
		
		this.transactionTable.setPlaceholder(new Label("There were no transactions in this period."));
		this.transactionList = FXCollections.observableArrayList();
		this.moreInformationButton.setVisible(false);
		this.expensesByCategoryTable.setRowFactory( tv -> {
		    TableRow<Row> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && !row.isEmpty()) {
		            this.handleDoubleClick(row.getItem());
		        }
		    });
		    return row ;
		});
		
		this.toggleButton(null);
		this.expensesByCategoryTable.getSelectionModel().selectedItemProperty().addListener(
			(Observable, oldValue, newValue) -> this.toggleButton(newValue));
		
		this.transactionDateColumn.setCellValueFactory(cellData -> cellData.getValue().dateToString());
		this.transactionAmountColumn.setCellValueFactory(cellData -> cellData.getValue().amountToString());
		this.transactionDescriptionColumn.setCellValueFactory(cellData -> cellData.getValue().getDescriptionProperty());
		this.transactionTable.setItems(transactionList);		
	}
	
	public StatsController() {
		this.fmt = new SimpleDateFormat("dd/MM/yyyy");
		this.calendar = new GregorianCalendar();
		this.balanceCalculator = new Balance();
		this.budget =  new Budget();
		this.tDAO = new TransactionDAO();
		this.ecDAO = new ExpenseCategoryDAO();
		this.cDAO = new CategoryDAO();
	}
	
	@FXML
	private void handlePreviousButton() {
		this.changeDate(true);
		this.updateData(this.periodChoiceBox.getSelectionModel().getSelectedItem(),
				this.statsTabPane.getSelectionModel().getSelectedItem());
	}
	
	@FXML
	private void handleNextButton() {
		this.changeDate(false);
		this.updateData(this.periodChoiceBox.getSelectionModel().getSelectedItem(),
				this.statsTabPane.getSelectionModel().getSelectedItem());
	}
	
	/**
	 * Updates the chart depending on which Period is selected
	 * @param period
	 */
	private void updateData(Period period, Tab selectedTab) {
		if (selectedTab.equals(this.balanceTab)) {
			this.populateLineChart(period);
		} else if (selectedTab.equals(this.expensesTab)) {
			this.populateExpensePieChart(period);
		} else if (selectedTab.equals(this.incomeTab)) {
			this.populateIncomePieChart(period);
		} else if (selectedTab.equals(this.expensesByCategoryTab)) {
			this.updateCategorySpendingTable(period);
		}
	}
	
	/**
	 * Populates the expense pie chart with data from a given period
	 * @param period
	 */
	private void populateExpensePieChart(Period period) {
		int accountID = this.mainApp.getLoggedInAs().getAccountID();
			
		try {
			HashMap<Integer, Double> categorySpendingMap = this.balanceCalculator.spendingByAllCategoryAndDate(accountID, calendar, period);		
			// remove old data
			ObservableList<ExpenseCategory> categoryList = this.ecDAO.getAllExpenseCategories(accountID);
			this.expensePieChartData.clear();
			for (Category c: categoryList) {
				if (categorySpendingMap.containsKey(c.getCategoryID())) {
					expensePieChartData.add(new PieChart.Data(c.getName(), categorySpendingMap.get(c.getCategoryID())));
				} 
			}	
			this.expensesPieChart.setData(expensePieChartData);
			this.expensesPieChart.getData().forEach(data -> data.nameProperty().set
					(data.getName() + " $" + String.format("%.2f (%.2f%%)", 
							data.getPieValue(), (data.getPieValue() / this.expensePieChartTotal.get() * 100)))
			);
			this.updatePieChartTitle(period);
		} catch (Exception e) {
			this.createDBErrorDialog(e.getMessage());
		}
	}
	
	/**
	 * Populates the income pie chart with data from a period
	 * @param period
	 */
	private void populateIncomePieChart(Period period) {
		int accountID = this.mainApp.getLoggedInAs().getAccountID();
		
		try {
			HashMap<Integer, Double> categorySpendingMap = this.balanceCalculator.spendingByAllCategoryAndDate(accountID, calendar, period);		
			ObservableList<Category> categoryList = this.cDAO.getIncomeCategories(accountID);

			this.incomePieChartData.clear();
			
			for (Category c: categoryList) {
				if (categorySpendingMap.containsKey(c.getCategoryID())) {
					incomePieChartData.add(new PieChart.Data(c.getName(), categorySpendingMap.get(c.getCategoryID())));
				} 
			}
			
			this.incomePieChart.setData(incomePieChartData);
			this.incomePieChart.getData().forEach(data -> data.nameProperty().set
					(data.getName() + " $" + String.format("%.2f (%.2f%%)", 
							data.getPieValue(), (data.getPieValue() / this.incomePieChartTotal.get() * 100)))
			);
			this.updatePieChartTitle(period);
		} catch (Exception e) {
			this.createDBErrorDialog(e.getMessage());
		}
	}
	
	/**
	 * Updates pie chart title
	 * @param period Weekly, Monthly or Yearly
	 */
	private void updatePieChartTitle(Period period) {
		// display chart title
		CalendarPair startEndDate = tDAO.getStartEndDate(this.calendar, period);
		startEndDate.getEnd().add(Calendar.DAY_OF_YEAR, -1);
		String periodStr = CalFormatter.toString(startEndDate.getStart()) + "-" + CalFormatter.toString(startEndDate.getEnd());
		Tab currentTab = this.statsTabPane.getSelectionModel().getSelectedItem();
		
		if (currentTab.equals(this.expensesTab)) {
			if (expensePieChartData.size() > 0) {
				this.expensesPieChart.titleProperty().set("Expense Breakdown from " + periodStr);
			} else {
				this.expensesPieChart.titleProperty().set("No expenses from " + periodStr);
			}
		} else if (currentTab.equals(this.incomeTab)) {
			if (incomePieChartData.size() > 0) {
				this.incomePieChart.titleProperty().set("Income Breakdown from " + periodStr);
			} else {
				this.incomePieChart.titleProperty().set("No income from " + periodStr);
			}
		}
	}
		
	/**
	 * Prepares function to generate data points
	 * @param period Weekly, Monthly or Yearly
	 */
	private void populateLineChart(Period period) {
		Calendar startDate;
		switch (period) {
		case DAILY:
			startDate = tDAO.getStartEndDate(this.calendar, Period.DAILY).getStart();
			// Get point for the day
			this.populateLineChart(startDate, 1, 1, Calendar.DAY_OF_YEAR);
			break;
		case WEEKLY:
			startDate = tDAO.getStartEndDate(this.calendar, Period.WEEKLY).getStart();
			// Get points for all 7 days in the week
			this.populateLineChart(startDate, 7, 1, Calendar.DAY_OF_YEAR);
			break;
		case MONTHLY:
			startDate = tDAO.getStartEndDate(this.calendar, Period.MONTHLY).getStart();
			this.populateLineChart(startDate, this.calendar.getMaximum(Calendar.DAY_OF_MONTH), 1, Calendar.DAY_OF_YEAR);			
			break;
		case YEARLY:
			startDate = tDAO.getStartEndDate(this.calendar, Period.YEARLY).getStart();
			// Get points for every 15 days
			this.populateLineChart(startDate, 25, 15, Calendar.DAY_OF_YEAR);			
			break;
		default:
			break;
		}
	}
	
	/**
	 * Populates the line chart with date and balance data
	 * @param c Calendar object holding the start date of the period
	 * @param dataPoints Number of points on the chart
	 * @param timeApart How much time each point on the x-axis is apart
	 * @param calIncrement How much time to increment each point on x-axis, e.g. week, month
	 */
	private void populateLineChart(Calendar c, int dataPoints, int timeApart, int calIncrement) {
    	int accountID = this.mainApp.getLoggedInAs().getAccountID();
	    XYChart.Series<String, Double> series = new XYChart.Series<String, Double>();
        
	    try {      	
        	for (int i = 0; i < dataPoints; i++) {
        		double balance = this.balanceCalculator.calculateTotalBalanceToDate(accountID, c);
        		String dateString = this.fmt.format(c.getTime());

        		// Print a label for X-axis every 'printXLabel' labels
			    // And last label on X-axis
        		XYChart.Data<String, Double> data = new XYChart.Data<String, Double>(dateString, balance);
        	    // Update balance label when hovering over a data point
        		data.setNode(
        	              new HoveredNode(dateString, balance, this.balanceLabel)
        	    );
        		series.getData().add(data);
        		c.add(calIncrement, timeApart);
        	}
        } catch (Exception e) {
        	this.createDBErrorDialog(e.getMessage());
        }
        
        // clear series from lineChart if it exists
        if (this.lineChart.getData().size() > 0) {
        	this.lineChart.getData().clear();
        }
        
        // populate lineChart with series
        this.lineChart.getData().add(series);
	}
	
	/** 
	 * Code adapted from: https://gist.github.com/jewelsea/4681797
	 * A node which updates a label on hover 
	 */
	class HoveredNode extends StackPane {
		HoveredNode(String dateString, double value, Label balanceLabel) {
			setPrefSize(10, 10);

			String balanceStr = "Balance: " + this.getBalanceStr(value);
			
			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override 
				public void handle(MouseEvent mouseEvent) {
					// update label text
					if (value > 0) {
						balanceLabel.setTextFill(Color.GREEN);
					} else if (value < 0) {
						balanceLabel.setTextFill(Color.RED);
					} else {
						balanceLabel.setTextFill(Color.ORANGE);
					}
					
					balanceLabel.setText(balanceStr);
					setCursor(Cursor.CROSSHAIR);
				}
			});
			
			setOnMouseExited(new EventHandler<MouseEvent>() {
				@Override 
				public void handle(MouseEvent mouseEvent) {
					balanceLabel.setText("Balance: ");
					getChildren().clear();
				}
			});
		}
				
		private String getBalanceStr(double value) {
			DecimalFormat df = new DecimalFormat("0.00");
			return ((value >= 0) ? "" : "-") + "$" + df.format(Math.abs(value));
		}
	}
	
	/**
	 * Updates the category spending table
	 * @param period 
	 */
	private void updateCategorySpendingTable(Period period) {
		int accountID = this.mainApp.getLoggedInAs().getAccountID();
		
		ObservableList<ExpenseCategory> expenseCategoryList = this.budget.getAllExpenseCategories(accountID);
		this.expenseTableData.clear();
		
		for (Category category : expenseCategoryList) {
			try {
				double amount = this.balanceCalculator.spendingByCategoryAndDate(category.getCategoryID(), this.calendar, period);
				DecimalFormat df = new DecimalFormat("0.00");
				String amountString = "$" + df.format(amount);
				if (amount > 0) {
					Row row = new Row(category.getName(), amountString, "", category.getCategoryID());
					this.expenseTableData.add(row);
				}
			} catch (Exception e) {
				this.createDBErrorDialog(e.getMessage());
			}
		}
		
		// update table's label as well
		this.updateCategorySpendingLabel(period);
	}
	
	/**
	 * Update the label for the category spending table. Called by updateCategorySpendingTable()
	 * @param period Spending period to generate a label for
	 */
	private void updateCategorySpendingLabel(Period period) {
		expenseTableTitle.setText("Category Spending From " + this.generatePeriodString(period));
	}
	
	/**
	 * Generates a string containing a date range in the format "Date-Date"
	 * @param period The period the date range spans for
	 * @return A string with a date range for the given period
	 */
	private String generatePeriodString(Period period) {
		CalendarPair startEndDate = tDAO.getStartEndDate(this.calendar, period);
		startEndDate.getEnd().add(Calendar.DAY_OF_YEAR, -1);

		return CalFormatter.toString(startEndDate.getStart()) + "-" + CalFormatter.toString(startEndDate.getEnd());
	}
	
	@FXML
	private void handleMoreInformation() {
		if (this.expensesByCategoryTable.isVisible() && this.expensesByCategoryTable.getSelectionModel().getSelectedIndex() >= 0) {
			this.showTransactions(true, true);
		} else {
			this.showTransactions(false, false);
		}
	}
	
	/**
	 * Display / hide transactions for the selected category
	 * @param show True to show transactions, else false
	 * @param populateTable True to populate the table with transactions, else false
	 */
	private void showTransactions(boolean show, boolean populateTable) {
		this.expensesByCategoryTable.setVisible(!show);
		this.transactionTable.setVisible(show);
		
		// only update transactionList if transactions being shown
		if (show && populateTable) {
			try {
				Row row = this.expensesByCategoryTable.getSelectionModel().getSelectedItem();
				transactionList = tDAO.searchByCategoryAndDate(
						row.getID().get(), this.calendar, this.periodChoiceBox.getSelectionModel().getSelectedItem());
				this.transactionTable.setItems(transactionList);		
			} catch (Exception e) {
				this.createDBErrorDialog(e.getMessage());
			}
		}
		
		String text = (show) ? "Less Information" : "More Information";
		this.moreInformationButton.setText(text);
		
		this.buttonGridPane.setVisible(!show);
	}
		
	/**
	 * Hides / unhides the "More Information" button depending on the tab selected
	 * @param isSelected If the expenses by category tab is selected
	 */
	private void toggleControls(boolean isSelected) {
		// show button if tab selected
		this.moreInformationButton.setVisible(isSelected);
		this.showTransactions(!isSelected, false);
		
		// reset everything if we move away from tab
		if (!isSelected) {
			this.showTransactions(false, false);
		}
	}
	
	/**
	 * Grey out "more information" button if no row selected
	 * @param row 
	 */
	private void toggleButton(Row row) {
		if (row == null || this.expensesByCategoryTable.getSelectionModel().getSelectedIndex() < 0) {
			this.moreInformationButton.setDisable(true);
		} else {
			this.moreInformationButton.setDisable(false);
		}
	}
	
	private void handleDoubleClick(Row row) {
		this.showTransactions(true, true);
	}
	
	private void toggleBalanceLabel(boolean isBalanceTab) {
		if (isBalanceTab) {
			this.balanceLabel.setVisible(true);
		} else {
			this.balanceLabel.setVisible(false);
		}
	}
	
	/**
	 * Modifies calendar depending on whether previous or next was pressed and the period
	 * @param isPreviousPressed
	 */
	private void changeDate(boolean isPreviousPressed) {
		int n = (isPreviousPressed) ? -1 : 1;
		
		switch (this.periodChoiceBox.getSelectionModel().getSelectedItem()) {
			case DAILY:
				this.calendar.add(Calendar.DAY_OF_YEAR, n);
				break;
			case WEEKLY:
				this.calendar.add(Calendar.WEEK_OF_YEAR, n);
				break;
			case MONTHLY:
				this.calendar.add(Calendar.MONTH, n);
				break;
			case YEARLY:
				this.calendar.add(Calendar.YEAR, n);
				break;
			default:
				break;
		}
	}
}