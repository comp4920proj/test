package controller;

import dao.AccountDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import model.Account;

public class EditProfileController extends Controller {
	@FXML
	private TextField oldPasswordField;
	@FXML
	private PasswordField newPasswordField;
	@FXML
	private PasswordField confirmPasswordField;
	
	@FXML
	private Button OKButton;
	private AccountDAO accountDAO;

	@FXML
	private void initialize () {      	
		this.accountDAO = new AccountDAO();
	}
	
	@FXML
	public void handleOK() {
		if (this.fieldsNotEmpty()) {
			if (!this.passwordFieldsMatch()) {
				this.createErrorDialog("Invalid Password","Old password is incorrect" , "Please enter old password");
			} else if (this.newPasswordFieldsMatch()) {
				this.createErrorDialog("Invalid Password","New password cannot be the same as old password" , "Please enter a new password");
			} else if (!this.isSufficientLength()) {
				this.createErrorDialog("Invalid Password", "Password too weak", "Password must be at least 4 characters long");
			} else if (!this.confirmPasswordFieldsMatch()) {
				this.createErrorDialog("Invalid Password","New password and confirmed password do not match" , "Please confirm new password");
			} else {
				this.mainApp.getLoggedInAs().setPassword(this.newPasswordField.getText());
				accountDAO.updateObject(this.mainApp.getLoggedInAs());
				this.getMainApp().showPage("ProfileView.fxml", new ProfileController());
			}
		} else {
			this.createErrorDialog("Empty Fields", "Fields cannot be empty", "Fill in all fields");
		}
		
		
	}

	/**
	 * Checks if old password matches account password 
	 * @return true if they match
	 */
	private boolean passwordFieldsMatch() {
		
		System.out.print(".." + this.mainApp.getLoggedInAs().getPassword());
		return this.oldPasswordField.getText().equals(this.mainApp.getLoggedInAs().getPassword());
	}

	/**
	 * Checks if old password and new password fields match
	 * @return true if they match
	 */
	private boolean newPasswordFieldsMatch() {
		return this.oldPasswordField.getText().equals(this.newPasswordField.getText());
	}
	
	
	/**
	 * Checks if password and confirm password fields match
	 * @return true if they match
	 */
	private boolean confirmPasswordFieldsMatch() {
		return this.newPasswordField.getText().equals(this.confirmPasswordField.getText());
	}

	/**
	 * Checks if fields are not empty
	 * @return true if there is text in all fields
	 */
	private boolean fieldsNotEmpty() {
		return !this.oldPasswordField.getText().isEmpty() &&
				!this.newPasswordField.getText().isEmpty() &&
				!this.confirmPasswordField.getText().isEmpty();
	}

	/**
	 * Checks if password and username has sufficient length
	 * @return true if it meets the conditions
	 */
	private boolean isSufficientLength() {
		final int length = 3;
		return this.oldPasswordField.getText().length() > length &&
				this.newPasswordField.getText().length() > length;
	}
	
	@FXML
	private void handleCancel() {
		this.getMainApp().showPage("ProfileView.fxml", new ProfileController());
	}
}
