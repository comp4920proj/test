package controller;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import mainApp.Budgeteer;

public abstract class Controller {
	// used to get a reference back to the root layout's primary stage
    protected Budgeteer mainApp;
    
    public void setMainApp (Budgeteer mainApp) {
        this.mainApp = mainApp;
    }
    
    public Budgeteer getMainApp() {
    	return this.mainApp;
    }
    
    /**
     * Creates an error dialog
     * @param title Title of the error dialog
     * @param header Header of the error dialog
     * @param content The message to display in the error dialog
     */
	public void createErrorDialog(String title, String header, String content) {
		this.createDialog(AlertType.WARNING, title, header, content);
	}
	
	public Optional<ButtonType> createConfirmationDialog(String title, String header, String content) {
		return this.createDialog(AlertType.CONFIRMATION, title, header, content);
	}
	
	/**
	 * Creates a dialog
	 * @param type Warning, confirmation, etc.
	 * @param title Title of dialog
	 * @param header Header of dialog
	 * @param content Content inside dialog
	 * @return
	 */
	private Optional<ButtonType> createDialog(AlertType type, String title, String header, String content) {
		Alert alert = new Alert(type);
		alert.initOwner(this.mainApp.getPrimaryStage());
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		
		return alert.showAndWait();
	}
	
	/**
	 * Creates an error dialog for DB exceptions
	 * @param content
	 */
	public void createDBErrorDialog(String content) {
		this.createErrorDialog("Database Error", "A problem has occurred with the database", content);
	}
}
