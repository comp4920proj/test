package controller;

import java.text.DecimalFormat;

import dao.AccountCategoryDAO;
import javafx.collections.ObservableList;
import javafx.util.StringConverter;
import model.Category;
import model.Transaction;

public class TransactionConverter extends StringConverter<Transaction>{

	private AccountCategoryDAO acDAO = new AccountCategoryDAO();
	
	//Decimal formatter
	DecimalFormat df = new DecimalFormat("0.00"); 
	
	@Override
	public Transaction fromString(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString(Transaction t) {
		// TODO Auto-generated method stub
		if (t != null) {
			String s = "$" + df.format(t.getAmount()) + ": ";
			
			ObservableList<Category> list = acDAO.getCategories(t.getAccountID());
			for (Category c : list) {
				if (c.getCategoryID() == t.getCategoryID()) {
					s += c.getName();
					break;
				}
			}
			return s;
		}
		return "";
	}
}
