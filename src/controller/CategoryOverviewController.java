package controller;

import java.util.Optional;

import dao.Budget;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Category;

public class CategoryOverviewController extends Controller {
	@FXML
	private TableView<Category> categoryTable;
	@FXML
	private TableColumn<Category, String> categoryNameColumn;
	@FXML
	private TableColumn<Category, String> categoryTypeColumn;
	@FXML
	private TextField searchTextField;
	@FXML
	private Button deleteButton;
	@FXML
	private Button editButton;
	
	private Budget budget;
	
	@FXML
	private void initialize() {
		this.budget = new Budget();
		
		this.categoryTypeColumn.setCellValueFactory(cellData -> cellData.getValue().getCategoryTypeString());
		this.categoryNameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		
		this.categoryTable.setRowFactory( tv -> {
		    TableRow<Category> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && !row.isEmpty()) {
		            this.handleDoubleClick(row.getItem());
		        }
		    });
		    return row ;
		});
		
		this.categoryTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> this.changeButtons());
		
		ObservableList<Category> categoryList = this.budget.getAllCategories(this.mainApp.getLoggedInAs().getAccountID());
		this.categoryTable.setItems(categoryList);
	}
	
	/**
	 * Changes page to view and edit a category if row is double-clicked
	 * @param rowData
	 */
	private void handleDoubleClick(Category rowData) {
        try {
        	boolean isExpense = this.budget.isExpenseCategory(rowData.getCategoryID());
        	// only allow editing if it's not an undeletable income category
        	if (isExpense || rowData.getIsDeletable()) {
        		this.mainApp.showPage("CategoryEditView.fxml", new CategoryEditController(rowData, isExpense));
        	}
    	} catch (Exception e) {
        	this.createDBErrorDialog(e.getMessage());
        }
	}
	
	@FXML
	private void handleAddButton() {
		this.mainApp.showPage("CategoryAddView.fxml", new CategoryAddController());
	}
		
	@FXML
	private void handleEditButton() {		
    	int selectedIndex = this.categoryTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedIndex >= 0) {
    		Category toEdit = this.categoryTable.getItems().get(selectedIndex);
    		
    		try {
    			boolean isExpense = this.budget.isExpenseCategory(toEdit.getCategoryID());
    			this.mainApp.showPage("CategoryEditView.fxml", new CategoryEditController(toEdit, isExpense));    		
    		} catch (Exception e) {
    			this.createDBErrorDialog(e.getMessage());
    		}
    	} else {
    		this.createErrorDialog("No Selection", "No Category Selected", "Select a category to edit");
    	}
	}
	
	@FXML
	private void handleDeleteButton() {
    	int selectedIndex = this.categoryTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedIndex >= 0) { 
    		Category toDelete = this.categoryTable.getItems().get(selectedIndex);
    		
    		// check if user confirmed
    		Optional<ButtonType> clicked = this.createConfirmationDialog("Confirm Action", "Confirm Delete", 
    				"Are you sure you want to delete this category and all its transactions?");
    			
    		if (clicked.get() == ButtonType.OK) {
	    		try {
	    			if (this.budget.isExpenseCategory(toDelete.getCategoryID())) {
	    				this.budget.deleteExpenseCategory(toDelete.getCategoryID());
	    			} else {
	    				this.budget.deleteCategory(toDelete.getCategoryID());
	    			}
	    		} catch (Exception e) {
	    			this.createDBErrorDialog(e.getMessage());
	    		}
	    		this.categoryTable.getItems().remove(selectedIndex);
    		}
    	} else {
    		this.createErrorDialog("No selection", "No category selected", "Select a category from the table");
    	}
	}
	
	@FXML
	private void handleSearchButton() {
    	ObservableList<Category> c = this.budget.searchAllCategories(
    			this.mainApp.getLoggedInAs().getAccountID(), this.searchTextField.getText()); 
		this.categoryTable.setItems(c);
	}
	
	private void changeButtons() {
		boolean isDeletable = this.categoryTable.getSelectionModel().getSelectedItem().getIsDeletable();
		
		this.changeDeleteButton(isDeletable);
		this.changeViewEditButton(isDeletable);
	}
	
	private void changeDeleteButton(boolean isDeletable) {
		if (!isDeletable) {
			this.deleteButton.setDisable(true);
		} else {
			this.deleteButton.setDisable(false);
		}
	}
	
	private void changeViewEditButton(boolean isDeletable) {
		int categoryID = this.categoryTable.getSelectionModel().getSelectedItem().getCategoryID();
		try {
			boolean isExpense = this.budget.isExpenseCategory(categoryID);
			if (!isExpense && !isDeletable) {
				this.editButton.setDisable(true);
			} else {
				this.editButton.setDisable(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}