package controller;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import model.Row;

import dao.AccountCategoryDAO;
import dao.Balance;
import dao.CategoryDAO;
import dao.ExpenseCategoryDAO;
import dao.TransactionDAO;
import dao.TransactionDAO.Period;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import model.Category;
import model.ExpenseCategory;
import model.Transaction;

public class HomePageController extends Controller {
	@FXML 
	private Label amount;
	
	@FXML
	private ChoiceBox<Transaction> favourite;
	
	@FXML
	private TableView<ExpenseCategory> limitSummaryTable;
	
	@FXML
    private TableColumn<ExpenseCategory, Double> barGraphColumn;
	
	@FXML
    private TableColumn<ExpenseCategory, String> nameColumn;
	
	@FXML
    private TableColumn<ExpenseCategory, Double> limitSumColumn;
	
	@FXML
	private TableView<Row> futureTable;
	
	@FXML
	private TableColumn<Row, String> futureAmount;
	
	@FXML
	private TableColumn<Row, String> futureDate;
	
	@FXML 
	private PieChart pieChart = new PieChart();
	
	@FXML
	private Label favAmount;
	
	@FXML
	private Label favCategory;
	
	@FXML
	private Label favDescription;
	
	@FXML
	private Label monthString;
	
	private ObservableList<Transaction> allFavourites = FXCollections.observableArrayList();
	private ObservableList<ExpenseCategory> allCategories = FXCollections.observableArrayList();
	private ObservableList<Transaction> allTransactions = FXCollections.observableArrayList();
	private ObservableList<Row> allFutureTrans = FXCollections.observableArrayList();
	private ExpenseCategoryDAO categories = new ExpenseCategoryDAO();
	private TransactionDAO tDAO = new TransactionDAO();
	private AccountCategoryDAO acDAO = new AccountCategoryDAO();
	private CategoryDAO cDAO= new CategoryDAO();
	private Balance balanceCalculator = new Balance();
	private HashMap categoryMap = new HashMap();
	
	//Decimal formatter
	DecimalFormat df = new DecimalFormat("0.00"); 
	
	@FXML
    public void initialize () {
		update();
		
		favAmount.setText("");
		favCategory.setText("");
		favDescription.setText("");
		favourite.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> { if (newValue != null) showTransaction(newValue);});
	}

	private void showTransaction(Transaction t) {
		if (t != null) {
			ObservableList<Category> list = acDAO.getCategories(this.mainApp.getLoggedInAs().getAccountID());
			for (Category c : list) {
				categoryMap.put(c.getName(), c);
				if (c.getCategoryID() == t.getCategoryID()) {
					favCategory.setText(c.getName());
					break;
				}
			}
			favAmount.setText("$" + df.format(t.getAmount()));
			favDescription.setText(t.getDescription());
		}
	}

	public void update() {
		updateFavouriteDropDown();
		updateCategories();
		updatePieChart();
		updateBalance();
		updateFuture();
	}
	
	private void updateFuture() {
		allFutureTrans.clear();
		futureTable.setStyle("-fx-table-cell-border-color: transparent;");
		allTransactions = tDAO.getAccountTransactions(this.mainApp.getLoggedInAs().getAccountID());
		java.sql.Date date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		List<Transaction> list = allTransactions;
		
		Collections.sort(list, new Comparator<Transaction>() {
            	@Override
            	public int compare(Transaction t1, Transaction t2) {
            		return t1.getDate().compareTo(t2.getDate());
            	}
        	}
        );
		for (Transaction t : list) {
			//If the current time is less than transaction date, show in table
			if (date.getTime() < t.getDate().getTime()) {
				String sAmount;
				if (t.isExpense()) {
					sAmount = "-$" + df.format(t.getAmount());
				} else {
					sAmount = "$" + df.format(t.getAmount());
				}
				String sDate   = t.getDate().toString();
				
				Row r = new Row(sAmount, sDate);
				allFutureTrans.add(r);
			}
		}
		
		futureAmount.setCellValueFactory(cellData -> cellData.getValue().getAmount());
		futureDate.setCellValueFactory(cellData -> cellData.getValue().getDate());
		futureTable.setItems(allFutureTrans);
	}

	private void updatePieChart() {
		ObservableList<PieChart.Data> pieData = FXCollections.observableArrayList();
		for (ExpenseCategory e : allCategories) {
			try {
				double amount = balanceCalculator.spendingByCategoryAndDate(e.getCategoryID(), Calendar.getInstance(), Period.MONTHLY);
				if (amount != 0.0) {
					pieData.add(new PieChart.Data(e.getName(), amount));
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		pieChart.setData(pieData);
		pieChart.setTitle("Expenditure");
		pieChart.getData().forEach(data -> data.nameProperty().bind(
			Bindings.concat(data.getName(), " $", df.format(data.getPieValue()))));
		
	}

	private void updateCategories() {
		allCategories = categories.getAllExpenseCategories(this.mainApp.getLoggedInAs().getAccountID());
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		
		barGraphColumn.setCellValueFactory(cellData -> cellData.getValue().getProgress().asObject());
		barGraphColumn.setCellFactory(
			new Callback<TableColumn<ExpenseCategory, Double>, TableCell<ExpenseCategory, Double>>(){
				@Override
				public TableCell<ExpenseCategory, Double> call(TableColumn<ExpenseCategory, Double> param){
						return new ProgressCell();
				}
			});
		
		limitSumColumn.setCellValueFactory(cellData -> cellData.getValue().getLimit().getMaxAmountProperty().asObject());
		limitSumColumn.setCellFactory(c -> new CurrencyCell());
		
		limitSummaryTable.setItems(allCategories);
	}

	public HomePageController() {
		
	}
	
	//Not really an update but is called to init the choicebox
	public void updateFavouriteDropDown() {
		//allFavouritesID.clear();
		
		try {
			allFavourites = tDAO.getFavouriteTransactions(this.mainApp.getLoggedInAs().getAccountID());
			// choose how to display to users
		} catch (Exception e) {
			this.createDBErrorDialog(e.getMessage());
		}
		
		favourite.setConverter(new TransactionConverter());
		favourite.setItems(allFavourites);
	}
	
	//Calls to update the balance if a favourite transaction was entered
	public void updateBalance() {
		try {
			double balance = this.balanceCalculator.calculateMonthlyBalance(this.mainApp.getLoggedInAs().getAccountID(), new GregorianCalendar());
			if (balance < 0) {
				amount.setText("-$" + df.format(-balance));
				amount.setTextFill(Color.RED);
			} else if (balance > 0) {
				amount.setText("$" + df.format(balance));
				amount.setTextFill(Color.GREEN);
			} else {
				amount.setText("$" + df.format(balance));
				amount.setTextFill(Color.ORANGE);
			}
			//amount.setText("$" + df.format(balance));
		} catch (Exception e) {
			this.createDBErrorDialog(e.getMessage());
		}
	}
	
	@FXML
	private void handleEnter() {
		Transaction t = favourite.getValue();
		if (t != null) {
			Calendar c = Calendar.getInstance();
			java.sql.Date date = new java.sql.Date(c.getTimeInMillis());
			
			Category category = cDAO.getObject(t.getCategoryID());
			if (category != null) {
				t.setCategoryName(category.getName());
			}
			
			
			if (t.isExpense()) {
				if (t.getCategoryName().equals("")) {
					this.mainApp.addTransaction(t.getAmount(), date, null, t.getDescription(), false, "Expense");
				} else {
					this.mainApp.addTransaction(t.getAmount(), date, (Category) categoryMap.get(t.getCategoryName()), t.getDescription(), false, "Expense", null, null);
				}
				
			} else {
				this.mainApp.addTransaction(t.getAmount(), date, (Category) categoryMap.get(t.getCategoryName()), t.getDescription(), false, "Income", null, null);
			}

			update();
			favAmount.setText("");
			favCategory.setText("");
			favDescription.setText("");
			//favourite.getSelectionModel().clearSelection();
		}
	}
}