package controller;

import java.sql.SQLException;
import javafx.scene.control.Button;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.Account;

public class ProfileController extends Controller {
	@FXML
	private Label usernameLabel;

	@FXML
	private Button EditButton;

	@FXML
	private void initialize() throws SQLException, ClassNotFoundException { 
		this.showProfileDetails(this.mainApp.getLoggedInAs());
	}
		
	/**
	 * Fills all text fields to show profile details.
	 */
	private void showProfileDetails(Account acc) {
		usernameLabel.setText(acc.getUsername());
	}
	
	@FXML
	public void handleEdit() {
		this.getMainApp().showPage("EditProfileView.fxml", new EditProfileController());
	}
	
	@FXML
	public void handleHome() {
		this.getMainApp().showPage("HomePageView.fxml", new HomePageController());
	}
}

	
