package controller;

import dao.AccountDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.Account;

public class LoginPageController extends Controller {
	@FXML
	private TextField userNameTextField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private Label statusLabel;
	@FXML
	private ImageView logoImageView;
	@FXML
	private GridPane loginGridPane;
	private AccountDAO accountDAO;

	
	public LoginPageController() {
		this.accountDAO = new AccountDAO();
		accountDAO.createTable();
	}
	
	@FXML
	private void initialize() {
		Image image = new Image(LoginPageController.class.getResourceAsStream("../images/logo2cropped.png"));
		this.logoImageView.setImage(image);		
	}
	
	@FXML
    private void handleLogin() throws Exception {	
		Account account = accountDAO.getObject(this.userNameTextField.getText());
		if (account != null && account.getPassword().equals(this.passwordField.getText())) {
			// let application know who logged in
			this.getMainApp().setLoggedInAs(account);
			
			this.statusLabel.setText("Login Successful!");
			this.statusLabel.setTextFill(Color.GREEN);
			
			// TODO: remove debug
			System.out.println(account.toString());
			
			this.mainApp.showNavBar();
			this.mainApp.showPage("HomePageView.fxml", new HomePageController());
		} else {
			this.statusLabel.setText("Incorrect username or password");
			this.statusLabel.setTextFill(Color.RED);
		}
		
    }
	
	@FXML
	private void handleRegisterButton() {
		this.mainApp.showPage("RegisterPageView.fxml");
	}
}