package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Account {
	private IntegerProperty accountID;
	private StringProperty email;
	private StringProperty username;
	private StringProperty password;
	
	public Account(String username, String password) {
		this.accountID = new SimpleIntegerProperty(-1);
		this.email = new SimpleStringProperty("test@test.com");
		this.username = new SimpleStringProperty(username);
		this.password = new SimpleStringProperty(password);
	}
	
	public String toString() {
		return "accountID: " + accountID.get() + 
				" | username: " + username.get() + 
				" | password: " + password.get();
	}
	
	public Account() {
		this("test_username", "test_password");
	}
	
	public int getAccountID() {
		return this.accountID.get();
	}
	
	public void setAccountID(int accountID) {
		this.accountID.set(accountID);
	}
	
	public IntegerProperty getAccountIDProperty() {
		return this.accountID;
	}
	
	public String getEmail() {
		return email.get();
	}
	
	public void setEmail(String email) {
		this.email.set(email);
	}
	
	public StringProperty getEmailProperty() {
		return email;
	}
		
	public String getUsername() {
		return username.get();
	}
	
	public void setUsername(String username) {
		this.username.set(username);
	}
	
	public StringProperty getUsernameProperty() {
		return username;
	}
	
	public String getPassword() {
		return password.get();
	}
	
	public void setPassword(String password) {
		this.password.set(password);
	}
	
	public StringProperty getPasswordProperty() {
		return password;
	}
}
