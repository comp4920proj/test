package model;

public class Expense extends Transaction {
	private ExpenseCategory expenseCategory;
	
	public Expense() {
		super();
		this.expenseCategory = null;
	}
	
	public ExpenseCategory getExpenseCategory() {
		return expenseCategory;
	}

	public void setExpenseCategory(ExpenseCategory expenseCategory) {
		this.expenseCategory = expenseCategory;
	}
}
