package model;

public class Income extends Transaction{	
	public Income() {
		super();
	}
	
	/**
	 * Copy constructor
	 * @param transaction
	 * @param incomeCategory
	 */
	public Income(Transaction transaction) {
		super.setTransactionID(transaction.getTransactionID());
		super.setAmount(transaction.getAmount());
		super.setDate(transaction.getDate());
		super.setDescription(transaction.getDescription());
		super.setFavourite(transaction.isFavourite());
	}
}
