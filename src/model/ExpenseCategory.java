package model;

import javafx.beans.property.DoubleProperty;

public class ExpenseCategory extends Category {
	private Limit limit;
	private DoubleProperty progress;
	
	/**
	 * Default constructor
	 * All expense categories will contain a limit object
	 */
	public ExpenseCategory() {
		super();
		this.limit = new Limit();
		this.setProgress(limit.getProgressProperty());
	}
		
	public ExpenseCategory(int id, String name, Limit limit) {
		super(id, name);
		if (limit != null ) {
			this.limit = new Limit();
			setProgress(limit.getProgressProperty());
		}
	}
	
	/**
	 * Copy constructor - need to modify if there are changes to Category or Limit
	 * @param category
	 * @param limit
	 */
	public ExpenseCategory(Category category, Limit limit) {
		this(category.getCategoryID(), category.getName(), limit);
	}
	
	/**
	 * Copy constructor
	 * @param category
	 */
	public ExpenseCategory(ExpenseCategory category) {
		this(category.getCategoryID(), category.getName(), category.getLimit());
	}

	public Limit getLimit() {
		return limit;
	}

	public void setLimit(Limit limit) {
		this.limit = limit;
	}
	
	public DoubleProperty getProgress() {
		this.limit.calculateProgress();
		return this.limit.getProgressProperty();
	}
	
	public DoubleProperty getCurrAmount() {
		limit.calculateAndSetCurrentAmount();
		return limit.getCurrentAmountProperty();
	}

	public void setProgress(DoubleProperty progress) {
		this.progress = progress;
	}
}
