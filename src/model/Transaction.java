package model;
import java.sql.Date;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Transaction {
	private IntegerProperty transactionID;
	private IntegerProperty accountID;
	private IntegerProperty categoryID;
	private StringProperty categoryName;
	private DoubleProperty amount;
	private SimpleObjectProperty<LocalDate> date;
	private StringProperty description;
	private BooleanProperty favourite;
	private BooleanProperty isExpense;
	private IntegerProperty priority;
	
	/**
	 * All transactions are expenses by default unless set otherwise
	 * Requires a valid accountID before inserting into DB.
	 */
	public Transaction() {
		this.transactionID = new SimpleIntegerProperty(-1);
		this.accountID = new SimpleIntegerProperty(-1);
		this.categoryID = new SimpleIntegerProperty(-1);
		this.categoryName = new SimpleStringProperty("");
		this.amount = new SimpleDoubleProperty(0);
		// set default date to current date
		this.date = new SimpleObjectProperty<LocalDate>();
		// we need to use the setter for date as its argument has type java.sql.Date
		java.sql.Date date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		this.setDate(date);
		this.description = new SimpleStringProperty("");
		this.favourite = new SimpleBooleanProperty(false);
		this.isExpense = new SimpleBooleanProperty(true);
		this.priority = new SimpleIntegerProperty(1);
	}
	
	/**
	 * Transaction constructor
	 * Requires a valid accountID before inserting into DB.
	 * @param transactionID 
	 * @param accountID Must be valid
	 * @param categoryID Must be valid
	 * @param amount
	 * @param date Must be a java.sqlDate
	 * @param description
	 * @param favourite
	 * @param isExpense
	 */
	public Transaction(int transactionID, int accountID, int categoryID, String categoryName, double amount, Date date,
			String description, boolean favourite, boolean isExpense) {
		
		this.transactionID = new SimpleIntegerProperty(transactionID);
		this.accountID = new SimpleIntegerProperty(accountID);
		this.categoryID = new SimpleIntegerProperty(categoryID);
		this.categoryName = new SimpleStringProperty(categoryName);
		this.amount = new SimpleDoubleProperty(amount);
		// set default date to current date
		this.date = new SimpleObjectProperty<LocalDate>();
		// we need to use the setter for date as its argument has type java.sql.Date
		this.setDate(date);
		this.description = new SimpleStringProperty(description);
		this.favourite = new SimpleBooleanProperty(favourite);
		this.isExpense = new SimpleBooleanProperty(isExpense);
		this.priority = new SimpleIntegerProperty(1);
	}
	
	/**
	 * Will insert into DB without a valid accountID and categoryID
	 */
	@Deprecated
	public Transaction(int transactionID, double amount, Date date, String description, boolean favourite) {
		this.transactionID = new SimpleIntegerProperty(transactionID);
		this.accountID = new SimpleIntegerProperty(-1);
		this.categoryID = new SimpleIntegerProperty(-1);
		this.amount = new SimpleDoubleProperty(amount);
		this.date = new SimpleObjectProperty<LocalDate>();
		this.setDate(date);
		this.description = new SimpleStringProperty(description);
		this.favourite = new SimpleBooleanProperty(favourite);
		this.isExpense = new SimpleBooleanProperty(true);
	}
		
	public int getTransactionID() {
		return transactionID.get();
	}

	public void setTransactionID(int transactionID) {
		this.transactionID.set(transactionID);
	}
	
	public IntegerProperty getTransactionIDProperty() {
		return transactionID;
	}
	
	public int getAccountID() {
		return accountID.get();
	}
	
	public void setAccountID(int accountID) {
		this.accountID.set(accountID);
	}
	
	public IntegerProperty getAccountIDProperty() {
		return accountID;
	}
	
	public int getCategoryID() {
		return categoryID.get();
	}
	
	public void setCategoryID(int categoryID) {
		this.categoryID.set(categoryID);
	}
	
	public IntegerProperty getCategoryIDProperty() {
		return categoryID;
	}
	
	public String getCategoryName() {
		return categoryName.get();
	}
	
	public void setCategoryName(String categoryName) {
		this.categoryName.set(categoryName);
	}
	
	public StringProperty getCategoryNameProperty() {
		return categoryName;
	}
	
	public StringProperty getTypeString() {
		SimpleStringProperty type = new SimpleStringProperty("");
		if (this.isExpense()) {
			type.set("Expense");
		} else {
			type.set("Income");
		}
		return type;
	}
	public StringProperty getFavouriteString() {
		SimpleStringProperty type = new SimpleStringProperty("");
		if (this.isFavourite()) {
			type.set("Yes");
		} else {
			type.set("No");
		}
		return type;
	}

	public double getAmount() {
		return amount.get();
	}
	
	public void setAmount(double amount) {
		this.amount.set(amount);
	}
	
	public DoubleProperty getAmountProperty() {
		return amount;
	}
	
	/**
	 * Returns a java.sql.Date object
	 * @return
	 */
	public Date getDate() {
		// Gets the LocalDate object from SimpleObjectProperty<LocalDate>
		// Converts it into an sql.Date object
		return Date.valueOf(this.date.get());
	}

	/**
	 * Converts a java.sql.Date object to a LocalDate
	 * @param date
	 */
	public void setDate(Date date) {
		this.date.set(date.toLocalDate());
	}
	
	public SimpleObjectProperty<LocalDate> getDateProperty() {
		return date;
	}
	
	public String getDescription() {
		return description.get();
	}
	
	public void setDescription(String description) {
		this.description.set(description); 
	}
	
	public StringProperty getDescriptionProperty() {
		return description;
	}
	
	public boolean isFavourite() {
		return favourite.get();
	}
	
	public void setFavourite(boolean favourite) {
		this.favourite.set(favourite); 
	}
	
	public BooleanProperty getIsFavouriteProperty() {
		return favourite;
	}
	
	public boolean isExpense() {
		return isExpense.get();
	}
	
	public void setIsExpense(boolean isExpense) {
		this.isExpense.set(isExpense);
	}
	
	public BooleanProperty getIsExpenseProperty() {
		return isExpense;
	}
	
	// integer value need to decide on range
	public void setPriority(int priority) {
		this.priority.set(priority);
	}
	
	public IntegerProperty getPriorityProperty() {
		return priority;
	}
	
	public int getPriority() {
		return priority.get();
	}
		
	public String toString() {
		String str = "id: " + this.transactionID + " | Date: " + this.date.toString() + " | Amount: " + this.amount + " | Description: " + this.description + " | Favourite: " + this.favourite + " | Category: " + this.categoryID + " | Expense: " + this.isExpense; 
		return str;
	}
	
	public StringProperty amountToString() {
		DecimalFormat df = new DecimalFormat("0.00");
		return new SimpleStringProperty("$" + df.format(getAmount()));
	}
	
	public StringProperty dateToString() {
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return new SimpleStringProperty(this.getDateProperty().get().format(formatters));
	}
}