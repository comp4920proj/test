package model;

import java.sql.Date;
import javafx.collections.ObservableList;
import dao.CategoryDAO;
import dao.TransactionDAO;

//holds all the transactions
public class Budget {
	private static TransactionDAO tDao = new TransactionDAO();
	private static CategoryDAO cDao = new CategoryDAO();
	
	public static void addTransaction (int accountID, Date date, Double amount, String category, String description, boolean favourite, String type) {
		System.out.println("CATEGORY TO ADD: " + category);
		if (type == "Expense") {
			Expense t = new Expense();
			t.setAccountID(accountID);
			t.setAmount(amount);
			t.setDate(date);
			if (category != null) {
				System.out.println("CATEGORY WAS SET");
				t.setCategoryName(category);
				System.out.println("ID is: " + cDao.getObjectByName(category,accountID).get(0).getCategoryID());
				t.setCategoryID(cDao.getObjectByName(category,accountID).get(0).getCategoryID());
			}
			t.setDescription(description);
			t.setFavourite(favourite);
			tDao.addObjectAndGetID(t);
		} else {
			Income t = new Income();
			t.setAccountID(accountID);
			t.setAmount(amount);
			t.setDate(date);
			if (category != null) {
				t.setCategoryName(category);
				System.out.println("ID is: " + cDao.getObjectByName(category,accountID).get(0).getCategoryID());
				t.setCategoryID(cDao.getObjectByName(category,accountID).get(0).getCategoryID());
			}
			t.setDescription(description);
			t.setFavourite(favourite);
			t.setIsExpense(false);
			tDao.addObjectAndGetID(t);
		}
	}
	
	public static void addTransaction (int accountID, Date date, Double amount, Category category, String description, boolean favourite, String type) {
		System.out.println("CATEGORY TO ADD: " + category.getName());
		if (type == "Expense") {
			Expense t = new Expense();
			t.setAccountID(accountID);
			t.setAmount(amount);
			t.setDate(date);
			if (category != null) {
				System.out.println("CATEGORY WAS SET");
				t.setCategoryName(category.getName());
				System.out.println("ID is: " + category.getCategoryID());
				t.setCategoryID(category.getCategoryID());
			}
			t.setDescription(description);
			t.setFavourite(favourite);
			tDao.addObjectAndGetID(t);
		} else {
			Income t = new Income();
			t.setAccountID(accountID);
			t.setAmount(amount);
			t.setDate(date);
			if (category != null) {
				t.setCategoryName(category.getName());
				System.out.println("ID is: " + category.getCategoryID());
				t.setCategoryID(category.getCategoryID());
			}
			t.setDescription(description);
			t.setFavourite(favourite);
			t.setIsExpense(false);
			tDao.addObjectAndGetID(t);
		}
	}
		
	// if string is empty, keep the initial values of the transaction
	public static void editTransaction (String id, String amount, Date date, String description, boolean favourite) {
		Transaction t = tDao.getObject(Integer.parseInt(id));
		if (!amount.isEmpty()) {t.setAmount(Double.parseDouble(amount));}

		t.setDate(date);
		
		// this one is pretty dumb, can't delete description (limitation of console app mainly) 
		if (!description.isEmpty()) {t.setDescription(description);}
		t.setFavourite(favourite);
		
		tDao.updateObject(t);
	}
	
	public static void editAmount(Transaction t, Double amount) {
		t.setAmount(amount);
		tDao.updateObject(t);
	}
	
	public static void editCategory(Transaction t, int categoryID) {
		t.setCategoryID(categoryID);
		tDao.updateObject(t);
	}
	
	public static void editDate(Transaction t, Date date) {
		t.setDate(date);
		tDao.updateObject(t);
	}
	
	public static void editDescription(Transaction t, String description) {
		t.setDescription(description);
		tDao.updateObject(t);
	}
	
	public static void editFavourite(Transaction t, boolean favourite) {
		t.setFavourite(favourite);
		tDao.updateObject(t);
	}
	
	public static void editTransaction (int id, Double amount, Date date, String description, boolean favourite) {
		Transaction t = tDao.getObject(id);
		if (t == null) {
			return;
		}
		if (amount != null) {t.setAmount(amount);}
		t.setDate(date); 
		t.setDescription(description);
		t.setFavourite(favourite);
		
		tDao.updateObject(t);
	}
	
	public static void deleteTransaction (String id) {
		if (!id.isEmpty()) {tDao.deleteObject(tDao.getObject(Integer.parseInt(id)));}
	}
	
	public static ObservableList<Category> getCategoryNames() {
		return (cDao.getAllObjects());
		
	}
	
	public static int getCategoryID(String categoryName, int accountID) {
		return cDao.getObjectByName(categoryName, accountID).get(0).getCategoryID();
	}
	
	// for testing console runner
	public static void addCategory(int transactionId,int accountId, String category){
		Transaction t = tDao.getObject(transactionId);		
		if (t == null) {
			return;
		}
		Category c = new Category(transactionId,accountId,category);
		int cID = cDao.addObjectAndGetID(c);
		t.setCategoryID(cID);
		tDao.updateObject(t);
	}
}
