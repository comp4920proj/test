package model;

import java.util.GregorianCalendar;

import dao.Balance;
import dao.TransactionDAO.Period;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Limit {
	private IntegerProperty limitID;
	private IntegerProperty categoryID;
	private DoubleProperty maxAmount;
	private Period period;
	// these fields are calculated at run-time, not stored in database
	private DoubleProperty currentAmount;
	private DoubleProperty progress;

	/**
	 * Use this constructor when initialising a Limit to insert into DB
	 * @param limitID Set to -1 for insertion into DB
	 * @param categoryID Must be a valid ID in category table
	 * @param maxAmount The limit's max amount
	 * @param period Whether the limit is DAILY, WEEKLY, MONTHLY or YEARLY
	 */
	public Limit(int categoryID, double maxAmount, Period period) {
		this.limitID = new SimpleIntegerProperty(-1);
		this.categoryID = new SimpleIntegerProperty(categoryID);
		this.maxAmount = new SimpleDoubleProperty(maxAmount);
		this.period =  period;

		this.currentAmount = new SimpleDoubleProperty(0);
		this.progress = new SimpleDoubleProperty(0);
	}
	
	public Limit(int categoryID) {
		this(categoryID, 0, Period.WEEKLY);
	}
	
	public Limit(int categoryID, double maxAmount) {
		this(categoryID, maxAmount, Period.WEEKLY);
	}
	
	/**
	 * Default constructor.
	 * Weekly limit with maxAmount = 0.
	 */
	public Limit() {
		this(-1, 0, Period.WEEKLY);
	}

	public int getLimitID() {
		return limitID.get();
	}

	public void setLimitID(int limitID) {
		this.limitID.set(limitID); 
	}
	
	public IntegerProperty getLimitIDProperty() {
		return this.limitID;
	}
	
	public int getCategoryID() {
		return categoryID.get();
	}

	public void setCategoryID(int categoryID) {
		this.categoryID.set(categoryID); 
	}
	
	public IntegerProperty getCategoryIDProperty() {
		return this.categoryID;
	}

	public double getMaxAmount() {
		return maxAmount.get();
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount.set(maxAmount); 
	}
	
	public DoubleProperty getMaxAmountProperty() {
		return this.maxAmount;
	}
	
	public void setPeriod(Period period) {
		this.period = period;
	}
	
	public Period getPeriod() {
		return this.period;
	}
	
	public StringProperty getPeriodProperty() {
		StringProperty ret = new SimpleStringProperty("");
		switch(this.period) {
			case DAILY:
				ret.set("Daily");
				break;
			case WEEKLY:
				ret.set("Weekly");
				break;
			case MONTHLY:
				ret.set("Monthly");
				break;
			case YEARLY:
				ret.set("Yearly");
				break;
			default:
				break;
		}
		return ret;
	}
	
	public double getCurrentAmount() {
		return currentAmount.get();
	}

	public void setCurrentAmount(double CurrentAmount) {
		this.currentAmount.set(CurrentAmount); 
	}
	
	public DoubleProperty getCurrentAmountProperty() {
		return this.currentAmount;
	}
	
	public double getProgress() {
		return progress.get();
	}

	public void setProgress(double Progress) {
		this.progress.set(Progress); 
	}
	
	public DoubleProperty getProgressProperty() {
		return this.progress;
	}
	
	public void calculateAndSetCurrentAmount() {
		Balance balance = new Balance();
		
		try {
			// calculates the current spending for the period specified in limit
			this.setCurrentAmount(balance.spendingByCategoryAndDate(this.getCategoryID(), new GregorianCalendar(), this.period));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void calculateProgress() {
		// calculate progress if maxAmount > 0
		if (this.getMaxAmount() > 0) {
			this.calculateAndSetCurrentAmount();
			// requires current amount to be positive for progress calculation
			this.setCurrentAmount(Math.abs(this.getCurrentAmount()));
			this.setProgress((this.getCurrentAmount() / this.getMaxAmount()));
		// else set to 0
		} else {
			this.setProgress(0);
		}
	}
}
