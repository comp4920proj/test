package model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Row {
	private SimpleStringProperty name;
	private SimpleStringProperty amount;
	private SimpleStringProperty date;
	private SimpleIntegerProperty id;
	
	public Row(String name, String amount, String date, int id) {
		this.setName(new SimpleStringProperty(name));
		this.setAmount(new SimpleStringProperty(amount));
		this.setDate(new SimpleStringProperty(date));
		this.setID(new SimpleIntegerProperty(id));
	}
	
	public Row(String s1, String s2){
		this.setAmount(new SimpleStringProperty(s1));
		this.setDate(new SimpleStringProperty(s2));
	}

	public SimpleStringProperty getName() {
		return name;
	}
	
	public void setName(SimpleStringProperty name) {
		this.name = name;
	}
	
	public SimpleStringProperty getAmount() {
		return amount;
	}

	public void setAmount(SimpleStringProperty amount) {
		this.amount = amount;
	}

	public SimpleStringProperty getDate() {
		return date;
	}

	public void setDate(SimpleStringProperty date) {
		this.date = date;
	}
	
	public SimpleIntegerProperty getID() {
		return id;
	}
	
	public void setID(SimpleIntegerProperty id) {
		this.id = id;
	}
}
