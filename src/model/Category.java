package model;

import dao.ExpenseCategoryDAO;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

// class will become abstract after income and expense categories implemented
public class Category {
	private IntegerProperty categoryID;
	private IntegerProperty accountID;
	private StringProperty name;
	// determine if the name is editable and category is deletable
	private BooleanProperty isEditable;	
	
	public Category(int categoryID, int accountID, String name) {
		this.categoryID = new SimpleIntegerProperty(categoryID);
		this.accountID = new SimpleIntegerProperty(accountID);
		this.name = new SimpleStringProperty(name);
		this.isEditable = new SimpleBooleanProperty(false);
	}
	
	public Category(int categoryID, int accountID, String name, boolean isEditable) {
		this.categoryID = new SimpleIntegerProperty(categoryID);
		this.accountID = new SimpleIntegerProperty(accountID);
		this.name = new SimpleStringProperty(name);
		this.isEditable = new SimpleBooleanProperty(isEditable);
	}
	
	public Category() {
		this(-1, -1, "");
	}
	
	public Category(int accountID, String name) {
		this(-1, accountID, name);
	}
	
	public Category(int accountID, String name, boolean isEditable) {
		this(-1, accountID, name, isEditable);
	}

	public Category(String name) {
		this(-1, -1, name);
	}
		
	@Deprecated // will remove function later
	public Category(String name, double limit) {
		this.categoryID = new SimpleIntegerProperty(-1);
		this.accountID = new SimpleIntegerProperty(-1);
		this.name = new SimpleStringProperty(name);
	}

	public int getCategoryID() {
		return categoryID.get();
	}

	public void setCategoryID(int categoryID) {
		this.categoryID.set(categoryID);
	}
	
	public IntegerProperty getCategoryIDProperty() {
		return this.categoryID;
	}
	
	public int getAccountID() {
		return accountID.get();
	}

	public void setAccountID(int accountID) {
		this.accountID.set(accountID);
	}
	
	public IntegerProperty getAccountIDProperty() {
		return this.accountID;
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name); 
	}
	
	public StringProperty getNameProperty() {
		return this.name;
	}
	
	public boolean getIsDeletable() {
		return isEditable.get();
	}

	public void setIsDeletable(boolean isEditable) {
		this.isEditable.set(isEditable); 
	}
	
	public BooleanProperty getIsDeletableProperty() {
		return this.isEditable;
	}
	
	public StringProperty getCategoryTypeString() {
		ExpenseCategoryDAO ecDAO = new ExpenseCategoryDAO();
		StringProperty str = new SimpleStringProperty("");
		try {
			if (ecDAO.isExpenseCategory(this.getCategoryID())) {
				str.set("Expense");
			} else {
				str.set("Income");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
}