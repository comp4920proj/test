package util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarPair {
	private Calendar start;
	private Calendar end;
	
	public CalendarPair() {
		this.start = new GregorianCalendar();
		this.end = new GregorianCalendar();
	}
	
	public CalendarPair(Calendar start, Calendar end) {
		this.start = start;
		this.end = end;
	}

	public Calendar getStart() {
		return start;
	}

	public void setStart(Calendar start) {
		this.start = start;
	}

	public Calendar getEnd() {
		return end;
	}

	public void setEnd(Calendar end) {
		this.end = end;
	}
}
