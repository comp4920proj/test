package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalFormatter {
	public static String toString(Calendar c) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(c.getTime());
	}
}
