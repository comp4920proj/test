package util;

import java.sql.*;
import java.util.ArrayList;

import com.sun.rowset.CachedRowSetImpl;

import dao.AccountDAO;
import dao.CategoryDAO;
import dao.LimitDAO;
import dao.TransactionDAO;

public class DBUtil {
	private static Connection connection = null;
	private static final String connectionURL = "jdbc:derby:BudgeteerDB;create=true";
    
	/**
	 * Connects to DB
	 */
    public static void connectDB() throws SQLException, ClassNotFoundException {
        try {
            connection = DriverManager.getConnection(connectionURL);
        } catch (Exception e) {
            System.out.println("Connection to database failed");
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * Disconnects from DB
     */
    public static void disconnectDB() throws SQLException {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception e){
            System.out.println("Disconnection from database failed");
        	e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * SELECT statements
     */
    public static ResultSet executeQueryDB(String query) throws SQLException, ClassNotFoundException {
        Statement stmt = null;
        ResultSet resultSet = null;
        /* 
         * CachedRowSet allows you to keep working on the data while disconnected
         * from the DB unlike ResultSet
         */
        CachedRowSetImpl crs = null;
        try {
        	/* debug */
            System.out.println("Select statement: " + query + "\n");

            DBUtil.connectDB();
            stmt = connection.createStatement();
            resultSet = stmt.executeQuery(query);
            crs = new CachedRowSetImpl();
            crs.populate(resultSet);
            
        } catch (Exception e) {
            System.out.println("executeQuery failed: " + e);
            throw e;
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            
            if (stmt != null) {
                stmt.close();
            }
            
            DBUtil.disconnectDB();
        }
        return crs;
    }
    
    
    /** 
     * INSERT/DELETE/UPDATE statements
     */
    public static void executeUpdateDB(String updateStmt) throws SQLException, ClassNotFoundException {
        Statement stmt = null;
        try {
            System.out.println("Update statement: " + updateStmt + "\n");
            DBUtil.connectDB();
            stmt = connection.createStatement();
            stmt.executeUpdate(updateStmt);
        } catch (Exception e) {            
        	System.out.println("executeUpdate failed: " + e);
            throw e;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            DBUtil.disconnectDB();
        }
    }
    
    /**
     *  Insert a row into a table and return ID(s) from the inserted row(s) 
     */
    public static ArrayList<Integer> insertAndGetID(String insertStmt) throws SQLException, ClassNotFoundException {
    	PreparedStatement pStmt = null;
    	ArrayList<Integer> insertedIDs = new ArrayList<Integer>();
    	
    	try {
    		DBUtil.connectDB();
    		
    		System.out.println("Insert statement: " + insertStmt);
    		
    		// grab the tableName from insert statement, it is always the 3rd word in the 
    		// split string
    		String tableName = insertStmt.split(" ")[2];
    		String idColumnName = tableName + "_ID";
    		    		
    		pStmt = connection.prepareStatement(insertStmt, new String[] { idColumnName });
    		pStmt.executeUpdate();
    		
    		ResultSet rs = pStmt.getGeneratedKeys();
    		while (rs.next()) {
    			insertedIDs.add(rs.getInt(1));
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw e;
    	} finally {
            if (pStmt != null) {
                pStmt.close();
            }
            DBUtil.disconnectDB();
        }
    	
		return insertedIDs;
    }
    
    /**
     * Initialises a single table in the database with a SQL create statement
     */
    public static void createTable(String sqlStmt) throws SQLException, ClassNotFoundException {
    	ResultSet rs;
    	
    	if (sqlStmt == null) {
    		System.out.println("DBUtil.dbInitTable: sqlStmt is null!");
    	}
    	
    	try {
    		DBUtil.connectDB();
    	
    		String[] split = sqlStmt.split("[^\\w]+");
    		
    		// extract tableName from create table statement
    		String tableName = (split.length > 2) ? split[2].toUpperCase() : null;
    		
    		rs = connection.getMetaData().getTables(null, null, tableName, null);
    		
    		System.out.println("Creating table <<" + tableName + ">>");
    		
    		if (!rs.next() && tableName != null) {
    			// disconnect as we are connecting in try statement
        		DBUtil.disconnectDB();
    			try {
    	        	System.out.println("Creating " + tableName + " table...");    		
    	            DBUtil.executeUpdateDB(sqlStmt);
    	        } catch (Exception e) {
    	            System.out.print("Error occurred during CREATE Operation: " + e);
    	            e.printStackTrace();
    	            throw e;
    	        }	
    		} else {
    			DBUtil.disconnectDB();
    		}
    	} catch (SQLException e) {
    		System.out.print("Error occured during getTables()");
    		throw e;
    	} 
    }
    
    /**
     * Removes all rows inside a given table
     * Restarts numbering of primary keys at 0
     * 
     * Note: Use this if you want to clear all records inside of a table
     */
    public static void clearTable(String tableName) throws SQLException, ClassNotFoundException {
    	try {    		
    		String deleteStmt = "DELETE FROM "+tableName+"";
    		String restartNumberingStmt = "ALTER TABLE "+tableName+" ALTER "+tableName+"_ID RESTART WITH 1";
    		
    		DBUtil.executeUpdateDB(deleteStmt);
    		DBUtil.executeUpdateDB(restartNumberingStmt);
    	} catch (Exception e) {
    		System.out.println("Could not clear table: " + tableName);
    		e.printStackTrace();
    		throw e;
    	}
    }
    
    /**
     * Drops a table from the database
     */
    public static void dropTable(String tableName) throws SQLException, ClassNotFoundException {
    	try {
    		System.out.println("Dropping " + tableName + "...");
    		DBUtil.executeUpdateDB("DROP TABLE " + tableName);
    	} catch (Exception e) {
    		System.out.println("Could not drop table: " + tableName);
    		e.printStackTrace();
    		throw e;
    	}
    }
    
    public static void shutdownDB() throws SQLException {
    	try {
        	System.gc();
        	DriverManager.getConnection(connectionURL + ";shutdown=true");
        
        // shutting down correctly always throws this exception, ignore exception
    	} catch (SQLNonTransientConnectionException e) {
        	System.out.println("Shutting down...");
        } catch (SQLException e) {
        	System.out.println("Shut down failed");
        	e.printStackTrace();
        	throw e;
        }
    }
    
    public static void createAllTables() throws Exception {
    	try {
    		AccountDAO aDAO = new AccountDAO();
    		TransactionDAO tDAO = new TransactionDAO();
    		CategoryDAO cDAO = new CategoryDAO();
    		LimitDAO lDAO = new LimitDAO();
    		
    		aDAO.createTable();
    		cDAO.createTable();
    		lDAO.createTable();
    		tDAO.createTable();
    	} catch (Exception e) {
    		throw e;
    	}
    }
    
    public static void destroyAllTables() throws Exception {
    	try {
    		DBUtil.dropTable("EVENT");
    		DBUtil.dropTable("LIMIT");
    		DBUtil.dropTable("CATEGORY");
    		DBUtil.dropTable("ACCOUNT");
    	} catch (Exception e) {
    		throw e;
    	}
    }
    
    public static void clearAllTables() throws Exception {
    	try {
    		DBUtil.clearTable("EVENT");
    		DBUtil.clearTable("LIMIT");
    		DBUtil.clearTable("CATEGORY");
    		DBUtil.clearTable("ACCOUNT");
    	} catch (Exception e) {
    		throw e;
    	}
    }
    
    public static void resetTables() throws Exception {
    	DBUtil.destroyAllTables();
    	DBUtil.createAllTables();
    }
}
