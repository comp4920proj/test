package util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

import controller.Controller;
import dao.CategoryDAO;
import dao.ExpenseCategoryDAO;
import dao.TransactionDAO;
import javafx.collections.ObservableList;
import model.ExpenseCategory;
import model.Transaction;

public class InputGenerator {
	/**
	 * Inserts transactions for 1 category that all cost $1
	 * @param accountID Account to insert transactions for
	 * @param nTransactions Number of transactions to insert
	 */
	public static void insertTestTransactions(int accountID, int nTransactions) {
		ExpenseCategoryDAO ecDAO = new ExpenseCategoryDAO();
		ObservableList<ExpenseCategory> ecList = ecDAO.getAllExpenseCategories(accountID);
		TransactionDAO tDAO = new TransactionDAO();
		
		// insert a lot of transactions for 1 category
		if (nTransactions > 0) {
			Calendar c = new GregorianCalendar();
			for (int i = 0; i < nTransactions; i++) {
				Transaction t = new Transaction();
				t.setAccountID(accountID);
				t.setCategoryID(ecList.get(0).getCategoryID());
				t.setAmount(1);
				
				c.add(Calendar.DAY_OF_YEAR, 1);
				
				t.setDate(new java.sql.Date(c.getTimeInMillis()));
				t.setIsExpense(true);
				
				tDAO.addObjectAndGetID(t);
			}
		}
	}
	
	public static void insertTransactions(int accountID, int nTransactions, int nCategory, int minAmount, int maxAmount, boolean isRandom) {
		ExpenseCategoryDAO ecDAO = new ExpenseCategoryDAO();
		ObservableList<ExpenseCategory> ecList = ecDAO.getAllExpenseCategories(accountID);
		TransactionDAO tDAO = new TransactionDAO();
		
		if (nTransactions > 0) {
			Calendar c = new GregorianCalendar();
			for (int i = 0; i < nTransactions; i++) {
				Transaction t = new Transaction();
				t.setAccountID(accountID);
				
				int categoryIndex = ThreadLocalRandom.current().nextInt(0, nCategory);
				t.setCategoryID(ecList.get(categoryIndex).getCategoryID());
				t.setDescription(Integer.toString(accountID));
				
				if (!isRandom) {
					t.setAmount(1);
				} else {
					int amount = ThreadLocalRandom.current().nextInt(minAmount, maxAmount + 1);
					t.setAmount(amount);
				}
				
				System.out.println(c.getTime());
				c.add(Calendar.DAY_OF_YEAR, 1);
				System.out.println(c.getTime());
				t.setDate(new java.sql.Date(c.getTimeInMillis()));
				t.setIsExpense(true);
				
				tDAO.addObjectAndGetID(t);
			}
		}
	}
	
	/**
	 * Provides the new account with some income and expense categories
	 * @param accountID The ID of the account to provide the categories
	 */
	public static void insertDefaultCategories(int accountID, Controller controller) {
		ExpenseCategoryDAO ecDAO = new ExpenseCategoryDAO();
		CategoryDAO cDAO = new CategoryDAO();
		
		try {
			ecDAO.addExpenseCategory("Food", 100, accountID, false);
			ecDAO.addExpenseCategory("Shopping", 100, accountID, false);
			ecDAO.addExpenseCategory("Housing", 100, accountID, false);
			ecDAO.addExpenseCategory("Transportation", 100, accountID, false);
			ecDAO.addExpenseCategory("Car", 100, accountID, false);
			ecDAO.addExpenseCategory("Entertainment", 100, accountID, false);
			ecDAO.addExpenseCategory("Communication", 100, accountID, false);
			ecDAO.addExpenseCategory("Investment", 100, accountID, false);
			ecDAO.addExpenseCategory("Other Expense", 100, accountID, false);
			
			cDAO.addObjectAndGetID("Income", accountID, false);
			cDAO.addObjectAndGetID("One-off", accountID, false);
			cDAO.addObjectAndGetID("Other Income", accountID, false);			
		} catch (Exception e) {
			controller.createDBErrorDialog(e.getMessage());
		}
	}
}
