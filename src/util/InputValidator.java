package util;

public class InputValidator {
	/**
	 * Checks that string contains a double
	 * @param str String to check if it contains a double
	 * @return True if the string contains a double
	 */
	public static boolean isDouble(String str) {
		// [0-9] at least 1 digit
		// [,.][0-9] optional comma or decimal with 1-2 digits
		return str.matches("[0-9]+([,.][0-9]{1,2})?");
	}
	
	/**
	 * Checks that string is a valid price
	 * @param str String to check if a valid price
	 * @return True if not empty and is a valid price
	 */
	public static boolean isPrice(String str) {
		return str.length() > 0 && InputValidator.isDouble(str);
	}
	
	/**
	 * Checks that the trimmed string is not empty
	 * @param str String to trim and check if empty
	 * @return True if trimmed string is empty
	 */
	public static boolean isNotEmptyString(String str) {
		return str.trim().length() > 0;
	}
}
