package mainApp;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Account;
import model.Budget;
import model.Category;
import model.ExpenseCategory;
import model.Transaction;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.List;

import controller.CategoryOverviewController;
import controller.Controller;
import controller.LoginPageController;
import dao.CategoryDAO;
import dao.ExpenseCategoryDAO;
import dao.TransactionDAO;

public class Budgeteer extends Application {
    
    // Create a primary stage that contains everything
	private static final double THRESHOLD = 0.8;
    private Stage primaryStage;
    private BorderPane rootLayout;
    private Account loggedInAs = null;
    private TransactionDAO tDao = new TransactionDAO();
    private CategoryDAO cDao = new CategoryDAO();
    private ExpenseCategoryDAO categories = new ExpenseCategoryDAO();
    
    public void setLoggedInAs(Account account) {
    	this.loggedInAs = account;
    }
    
    public Account getLoggedInAs() {
    	return this.loggedInAs;
    }
    
    public Stage getPrimaryStage() {
    	return this.primaryStage;
    }
        
    @Override
    public void start(Stage primaryStage) {
    	
        this.primaryStage = primaryStage;
        
        //Optional: Set a title for primary stage
        this.primaryStage.setTitle("Budgeteer");
        this.primaryStage.getIcons().add(new Image(Budgeteer.class.getResourceAsStream("../images/icon1.png")));
    	
        //2) Initialize RootLayout
        initRootLayout();

        //3) Display the homepage View
        this.showPage("LoginPageView.fxml", new LoginPageController());
    }
    
    //Initializes the root layout.
    public void initRootLayout() {
        try {
            //First, load root layout from RootLayout.fxml
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Budgeteer.class.getResource("../view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            //Second, show the scene containing the root layout.
            Scene scene = new Scene(rootLayout, 1024, 512); //We are sending rootLayout to the Scene.
            primaryStage.setScene(scene); //Set the scene in primary stage.

            //Third, show the primary stage
            primaryStage.show(); //Display the primary stage
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Displays the navBar
     */
    public void showNavBar() {
    	try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Budgeteer.class.getResource("../view/" + "NavBarView.fxml"));
            AnchorPane view = (AnchorPane) loader.load();
            
            // display view
            rootLayout.setLeft(view);
            
            // set controller's mainApp to Budgeteer
            Controller c = (Controller) loader.getController();
            c.setMainApp(this);
    	} catch (Exception e) {
    		
    	}
    }
            
    /**
     * Displays the view given by viewFileName
     * @param viewFileName
     */
    public void showPage(String viewFileName) {
        try {        	
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Budgeteer.class.getResource("../view/" + viewFileName));
            AnchorPane view = (AnchorPane) loader.load();
            
            // display view
            rootLayout.setCenter(view);
            
            // set controller's mainApp to Budgeteer
            Controller c = (Controller) loader.getController();
            c.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showTestPage() {
    	try {        	
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Budgeteer.class.getResource("../view/TestView.fxml"));
            GridPane view = (GridPane) loader.load();
            
            // display view
            rootLayout.setCenter(view);
            
            // set controller's mainApp to Budgeteer
            Controller c = (Controller) loader.getController();  
            c.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Use this if you need mainApp to be set before initialize is called
     * @param viewFileName
     * @param controller
     */
    public void showPage(String viewFileName, Controller controller) {
        try {        	
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Budgeteer.class.getResource("../view/" + viewFileName));
            
            // give controller a reference to Budgeteer
            controller.setMainApp(this);
            loader.setController(controller);
            
            AnchorPane view = (AnchorPane) loader.load();
            
            // display view
            rootLayout.setCenter(view);            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Displays the category overview page
     */
    public void showCategoryOverviewPage() {
		this.showPage("CategoryOverviewView.fxml", new CategoryOverviewController());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void deleteTransaction(Transaction t) {
    	Budget.deleteTransaction("" + t.getTransactionID());
    }

	public ObservableList<ExpenseCategory> getAllCategories() {
		return categories.getAllExpenseCategories(loggedInAs.getAccountID());
	}
	
	public ObservableList<Category> getIncomeCategories() {
		return cDao.getIncomeCategories(loggedInAs.getAccountID());
	}
	
	public int getCategoryID (String categoryName, int accountID) {
		return Budget.getCategoryID(categoryName, accountID);
	}
	
	public List<Transaction> getTransactions() {
		List<Transaction> transactions = tDao.getAccountTransactions(loggedInAs.getAccountID());
		for (Transaction t : transactions) {
			Category category = cDao.getObject(t.getCategoryID());
			if (category != null) {
				System.out.println("NAME IS: " + category.getName());
				t.setCategoryName(category.getName());
			}
		}
		return transactions;
	}
	
	public boolean hasReachedLimit(Category category) {
		try {
			ExpenseCategory ec = (ExpenseCategory) category;
			DoubleProperty progress = ec.getProgress();
			if (progress.doubleValue() > THRESHOLD) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void addTransaction(double newAmount, Date newDate, Category category, String newDescription, boolean favourite, String type, String repeatPeriod, Date repeatUntil) {
		// TODO Auto-generated method stub
		//CALL CALL ADD TRANSACTION HERE
		int accID = loggedInAs.getAccountID();
		if (repeatUntil == null) {
			Budget.addTransaction(accID, newDate, newAmount, category, newDescription, favourite, type);
			return;
		}
		
		
		LocalDate endDate = repeatUntil.toLocalDate();
		LocalDate updatedDate = newDate.toLocalDate();
		
		switch (repeatPeriod) {
			case (("Repeat?")):
				Budget.addTransaction(accID, newDate, newAmount, category, newDescription, favourite, type);
				break;
			case ("Daily"):
				while (!updatedDate.isAfter(endDate)) {
					Calendar c = Calendar.getInstance();
					c.setTime(Date.valueOf(updatedDate));
					Date date = new java.sql.Date(c.getTimeInMillis());
					Budget.addTransaction(accID, date, newAmount, category, newDescription, favourite, type);
					if (favourite) {
						favourite = false;
					}
					updatedDate = updatedDate.plusDays(1);
				}
				break;
			case ("Weekly"):
				while (!updatedDate.isAfter(endDate)) {
					Calendar c = Calendar.getInstance();
					c.setTime(Date.valueOf(updatedDate));
					Date date = new java.sql.Date(c.getTimeInMillis());
					Budget.addTransaction(accID, date, newAmount, category, newDescription, favourite, type);
					if (favourite) {
						favourite = false;
					}
					updatedDate = updatedDate.plusWeeks(1);
				}
				break;
			case ("Fortnightly"):
				while (!updatedDate.isAfter(endDate)) {
					Calendar c = Calendar.getInstance();
					c.setTime(Date.valueOf(updatedDate));
					Date date = new java.sql.Date(c.getTimeInMillis());
					Budget.addTransaction(accID, date, newAmount, category, newDescription, favourite, type);
					if (favourite) {
						favourite = false;
					}
					updatedDate = updatedDate.plusWeeks(2);
				}
				break;
			case ("Monthly"):
				while (!updatedDate.isAfter(endDate)) {
					Calendar c = Calendar.getInstance();
					c.setTime(Date.valueOf(updatedDate));
					Date date = new java.sql.Date(c.getTimeInMillis());
					Budget.addTransaction(accID, date, newAmount, category, newDescription, favourite, type);
					if (favourite) {
						favourite = false;
					}
					updatedDate = updatedDate.plusMonths(1);
				}
				break;
			case ("Yearly"):
				while (!updatedDate.isAfter(endDate)) {
					Calendar c = Calendar.getInstance();
					c.setTime(Date.valueOf(updatedDate));
					Date date = new java.sql.Date(c.getTimeInMillis());
					Budget.addTransaction(accID, date, newAmount, category, newDescription, favourite, type);
					if (favourite) {
						favourite = false;
					}
					updatedDate = updatedDate.plusYears(1);
				}
				break;
		}
		
	}

	public List<Transaction> getSpecificTransactions(LocalDate start, LocalDate end, String description, 
			String category) {
		
		int accountID = loggedInAs.getAccountID();
		int categoryID = (cDao.getObjectByName(category, accountID).size() > 0) ? cDao.getObjectByName(category, accountID).get(0).getCategoryID() : -1;
		Date dStart = null;
		Date dEnd = null;
		
		dStart = (start != null) ? toDate(start) : null;
		dEnd = (end != null) ? toDate(end) : null;
		try {
			List<Transaction> transactions = tDao.searchTransactions(description, dStart, dEnd, categoryID, accountID);
			for (Transaction t : transactions) {
				Category c = cDao.getObject(t.getCategoryID());
				if (c != null) {
					System.out.println("NAME IS: " + c.getName());
					t.setCategoryName(c.getName());
				}
			}
			return transactions;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return getTransactions();
		}
	}
	
	public static Calendar toCalendar(LocalDate localDate){ 
		  Calendar cal = Calendar.getInstance();
		  java.util.Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		  cal.setTime(date);
		  return cal;
	}
	
	public static Date toDate(LocalDate localDate){ 
			Date date = Date.valueOf(localDate);
		  return date;
	}

	public void editCategory(String categoryName, int accountID) {
		// TODO Auto-generated method stub
		System.out.println(cDao.getObjectByName(categoryName, accountID).get(0));
	}

	public void addTransaction(Double amount, Date date, String category, String description, boolean b, String type) {
		// TODO Auto-generated method stub
		int accID = loggedInAs.getAccountID();
		Budget.addTransaction(accID, date, amount, category, description, b, type);
		return;
	}
	
	
}