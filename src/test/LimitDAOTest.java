package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import dao.AccountDAO;
import dao.CategoryDAO;
import dao.LimitDAO;
import dao.ObjectDAO;
import dao.TransactionDAO.Period;
import model.Account;
import model.Category;
import model.Limit;
import util.DBUtil;

public class LimitDAOTest {
	@BeforeClass
	public static void setUpClass() {
		try {
			DBUtil.resetTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ObjectDAO<Limit> lDAO = new LimitDAO();
		lDAO.createTable();
	}
	
	@Test
	public void testInsertAndGet() {
		LimitDAO lDAO = new LimitDAO();
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();

		// insert account
		int accountID = aDAO.addObjectAndGetID(new Account());
		Account a = aDAO.getObject(accountID);
		assertNotEquals(a, null);
		
		// insert category
		Category c1 = new Category("DRUG MONEY");
		c1.setAccountID(accountID);
		int categoryID = cDAO.addObjectAndGetID(c1);		
		Category c = cDAO.getObject(categoryID);
		assertNotEquals(c, null);
		
		// insert limit
		Limit l = new Limit();
		l.setMaxAmount(420);
		l.setCategoryID(c.getCategoryID());
		lDAO.addObject(l);
		
		Limit l_copy = lDAO.getObject(1);
		assertTrue(l_copy.getMaxAmount() == l.getMaxAmount());
		
		cleanUp();
	}
	
	@Test
	public void testUpdate() {	
		final double newAmount = 69;
		LimitDAO lDAO = new LimitDAO();
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();

		// insert account
		int accountID = aDAO.addObjectAndGetID(new Account());
		Account a = aDAO.getObject(accountID);
		assertNotEquals(a, null);
		
		// insert category
		Category c1 = new Category("DRUG MONEY");
		c1.setAccountID(accountID);
		int categoryID = cDAO.addObjectAndGetID(c1);		
		Category c = cDAO.getObject(categoryID);
		assertNotEquals(c, null);
		
		// insert limit
		Limit l = new Limit();
		l.setCategoryID(categoryID);
		l.setMaxAmount(420);
		lDAO.addObject(l);
				
		// update
		l = lDAO.getObject(1);
		l.setMaxAmount(newAmount);
		l.setPeriod(Period.MONTHLY);
		lDAO.updateObject(l);
		
		Limit l_updated = lDAO.getObject(1);
		assertEquals(l_updated.getMaxAmount(), newAmount, 0);
		assertEquals(l_updated.getPeriod(), Period.MONTHLY);
		cleanUp();	
	}
	
	@Test
	public void testGetAll() {				
		LimitDAO lDAO = new LimitDAO();
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		final int n = 10;

		// insert account
		int accountID = aDAO.addObjectAndGetID(new Account());
		Account a = aDAO.getObject(accountID);
		assertNotEquals(a, null);
		
		// insert category
		Category c1 = new Category("DRUG MONEY");
		c1.setAccountID(accountID);
		int categoryID = cDAO.addObjectAndGetID(c1);		
		Category c = cDAO.getObject(categoryID);
		assertNotEquals(c, null);
		
		// insert limit
		Limit l = new Limit();
		l.setCategoryID(c.getCategoryID());
		for (int i = 0; i < n; i++) {
			lDAO.addObject(l);
		}
		
		List<Limit> limitList = lDAO.getAllObjects();
		assertTrue(limitList.size() == n);
		
		for (int i = 0; i < n; i++) {
			assertTrue(limitList.get(i).getLimitID() == i+1);
		}
		
		cleanUp();
	}
	
	@Test
	public void testDelete() {
		// TODO
	}
	
	/**
	 * Clear all entries from LIMIT table
	 */
	public void cleanUp() {
		try {
			DBUtil.clearAllTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
