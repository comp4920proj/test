package test;

import static org.junit.Assert.*;

import org.junit.Test;
import model.Transaction;

public class TransactionTest {
	@Test
	public void testConstructors() {
		Transaction t1 = new Transaction();
		
		assertTrue(t1.getTransactionID() == -1);
		assertTrue(t1.getAmount() == 0);
		assertTrue(t1.getDescription().equals(""));
		assertTrue(t1.isFavourite() == false);
	}
	
	@Test
	public void testConstructorNull() {
		Transaction t1 = new Transaction();
		
		assertTrue(t1.getAmountProperty() != null);
		assertTrue(t1.getDateProperty() != null);
		assertTrue(t1.getDescriptionProperty() != null);
		assertTrue(t1.getIsFavouriteProperty() != null);
		assertTrue(t1.getTransactionIDProperty() != null);
	}
}
