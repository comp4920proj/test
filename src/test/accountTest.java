package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import dao.AccountDAO;
import model.Account;
import util.DBUtil;

public class accountTest {
	@BeforeClass
	public static void setUpClass() {
	    //executed only once, before the first test
		//clear table if it exists
		try {
			DBUtil.clearTable("ACCOUNT");
		} catch (Exception e) {
			System.out.println("accountTest: clearing ACCOUNT table");
		}
		
		AccountDAO aDAO = new AccountDAO();
		aDAO.createTable();
	}
	
	@Test
	public void testInsertAndGet() {
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		final String username = "user";
		final String email = "kek@kek.com";

		a.setUsername(username);
		a.setEmail(email);
		
		aDAO.addObjectAndGetID(a);

		Account a_copy = aDAO.getObject(1);
		assertTrue(a_copy.getUsername().equals(username));
		assertTrue(a_copy.getEmail().equals(email));
		
		cleanUp();
	}
	
	@Test
	public void testUpdate() {
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		final String username = "user1";
		final String email = "kek@kek.com";
		
		aDAO.addObjectAndGetID(a);
		
		Account a_copy = aDAO.getObject(1);
		assertFalse(a_copy.getEmail().equals(email));
		
		a_copy.setUsername(username);
		a_copy.setEmail(email);
		aDAO.updateObject(a_copy);
		
		assertTrue(a_copy.getUsername().equals(username));
		assertTrue(a_copy.getEmail().equals(email));
		
		cleanUp();
	}
	
	@Test
	public void testGetAll() {
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		final int n = 10;
		
		for (int i = 0; i < n; i++) {
			a.setUsername("user_" + i);
			aDAO.addObjectAndGetID(a);
		}
		
		List<Account> l = aDAO.getAllObjects();
		assertTrue(l.size() == n);
		
		cleanUp();
	}
	
	@Test
	public void testDelete() {
		// TODO
	}
	
	public void cleanUp() {
		try {
			DBUtil.clearTable("ACCOUNT");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
