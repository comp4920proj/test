package test;

import static org.junit.Assert.*;


import org.junit.Test;
import model.Category;

public class CategoryTest {
	@Test
	public void TestConstructors() {
		Category c1 = new Category();
		
		assertEquals(c1.getCategoryID(), -1);
		assertEquals(c1.getName(), "");
		
		Category c2 = new Category(1, 1, "test");
		
		assertEquals(c2.getCategoryID(), 1);
		assertEquals(c2.getAccountID(), 1);
		assertEquals(c2.getName(), "test");
		
		Category c3 = new Category("test");
		
		assertEquals(c3.getCategoryID(), -1);
		assertEquals(c3.getAccountID(), -1);
		assertEquals(c3.getName(), "test");
	}
}
