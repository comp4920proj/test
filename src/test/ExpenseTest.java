package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Expense;

public class ExpenseTest {

	@Test
	public void testExpenseConstructor() {
		Expense e1 = new Expense();
		
		assertEquals(e1.getTransactionID(), -1);
		assertEquals(e1.getAmount(), 0, 0);
		assertEquals(e1.getDescription(), "");
		assertEquals(e1.isFavourite(), false);
		assertEquals(e1.getExpenseCategory(), null);
	}

}
