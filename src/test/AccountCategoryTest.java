package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import dao.AccountDAO;
import dao.CategoryDAO;
import dao.AccountCategoryDAO;
import model.Account;
import model.Category;
import util.DBUtil;

public class AccountCategoryTest {

	@Test
	public void test() {
		AccountCategoryDAO acDAO = new AccountCategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		CategoryDAO cDAO = new CategoryDAO();
				
		cleanUp();
		try {
			DBUtil.clearAllTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		aDAO.createTable();
		cDAO.createTable();
		
		// insert accounts
		int accountID1 = aDAO.addObjectAndGetID(new Account("lol", "xd"));
		int accountID2 = aDAO.addObjectAndGetID(new Account("lol2", "xd2"));
		
		// insert categories into above accounts
		Category cat = new Category("OLD MEMES");
		cat.setAccountID(accountID1);
		
		int categoryID1 = cDAO.addObjectAndGetID(cat); 
		cat.setAccountID(accountID2);
		
		int categoryID2 = cDAO.addObjectAndGetID(cat);
		int categoryID3 = cDAO.addObjectAndGetID(cat);
		
		List<Category> cList = cDAO.getAllObjects();
		for (Category c: cList) {
			System.out.println("id: " + c.getCategoryID());
		}
		
		// get inserted categories
		Category c1 = cDAO.getObject(categoryID1);
		Category c2 = cDAO.getObject(categoryID2);
		Category c3 = cDAO.getObject(categoryID3);
		
		assertNotEquals(c1, null);
		assertNotEquals(c2, null);
		assertNotEquals(c3, null);
		
		// check getCategories is working
		assertTrue(acDAO.getCategories(accountID1).size() == 1);
		assertTrue(acDAO.getCategories(accountID2).size() == 2);
		assertTrue(acDAO.getCategories(-1).size() == 0);
	}
	
	public void cleanUp() {
		try {
			DBUtil.clearTable("EVENT");
			DBUtil.clearTable("ACCOUNT");
			DBUtil.clearTable("CATEGORY");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
