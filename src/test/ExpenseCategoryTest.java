package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Category;
import model.ExpenseCategory;
import model.Limit;

public class ExpenseCategoryTest {
	@Test
	public void testConstructors() {
		ExpenseCategory ec1 = new ExpenseCategory();
		Category c1 = new Category();
		
		assertEquals(ec1.getCategoryID(), c1.getCategoryID());
		assertEquals(ec1.getName(), c1.getName());
		assertEquals(ec1.getLimit().getLimitID(), new Limit().getLimitID());
		
		Limit l1 = new Limit();
		ExpenseCategory ec2 = new ExpenseCategory(c1, l1);
		
		assertEquals(ec2.getCategoryID(), c1.getCategoryID());
		assertEquals(ec2.getName(), c1.getName());
		assertEquals(ec2.getLimit().getLimitID(), l1.getLimitID());
		assertEquals(ec2.getLimit().getMaxAmount(), l1.getMaxAmount(), 0);
		
		ExpenseCategory ec3 = new ExpenseCategory(1, "test", l1);
		assertEquals(ec3.getCategoryID(), 1);
		assertEquals(ec3.getName(), "test");
		assertEquals(ec3.getLimit().getLimitID(), l1.getLimitID());
		assertEquals(ec3.getLimit().getMaxAmount(), l1.getMaxAmount(), 0);
	}
}
