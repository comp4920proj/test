package test;

import static org.junit.Assert.*;
import org.junit.Test;
import dao.TransactionDAO.Period;
import model.Limit;

public class LimitTest {
	@Test
	public void testConstructor() {
		Limit l1 = new Limit();
		
		assertEquals(l1.getLimitID(), -1);
		assertEquals(l1.getCategoryID(), -1);
		assertEquals(l1.getMaxAmount(), 0, 0);
		assertEquals(l1.getPeriod(), Period.WEEKLY);
		
		Limit l2 = new Limit(1, 100.0, Period.DAILY);
		
		assertEquals(l2.getCategoryID(), 1);
		assertEquals(l2.getLimitID(), -1);
		assertEquals(l2.getMaxAmount(), 100.0, 0);
		assertEquals(l2.getPeriod(), Period.DAILY);
	}
}
