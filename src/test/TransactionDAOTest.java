package test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.sql.Date;
import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;

import dao.AccountDAO;
import dao.CategoryDAO;
import dao.ObjectDAO;
import dao.TransactionDAO;
import model.Account;
import model.Category;
import model.Transaction;
import util.DBUtil;

public class TransactionDAOTest {
	@BeforeClass
	public static void setUpClass() {
	    //executed only once, before the first test
		//clear table if it exists
		try {
			DBUtil.destroyAllTables();
			DBUtil.createAllTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ObjectDAO<Account> aDAO = new AccountDAO();
		ObjectDAO<Transaction> eDAO = new TransactionDAO();
		aDAO.createTable();
		eDAO.createTable();		
	}
	
	@Test
	public void testInsertAndGet() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("quick");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		tDAO.addObjectAndGetID(t);
		
		t.setAmount(420);
		
		tDAO.addObjectAndGetID(t);
		
		assertTrue(tDAO.getObject(1).getAmount() == 0);
		assertTrue(tDAO.getObject(2).getAmount() == 420);
		assertTrue(tDAO.getObject(1).isExpense() == true);
		
		cleanUp();
	}
	
	@Test
	public void testUpdate() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("lal");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		tDAO.addObjectAndGetID(t);
		
		Transaction t_copy = tDAO.getObject(1);
		
		assertTrue(t_copy.getAmount() == 0);
		
		t_copy.setAmount(420);
		
		tDAO.updateObject(t_copy);
		
		assertTrue(tDAO.getObject(1).getAmount() == 420);
		
		cleanUp();
	}
	
	@Test
	public void testSearchByDay() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		Calendar c = new GregorianCalendar();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("lul");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		c.set(Calendar.YEAR, 2000);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		t.setDate(new Date(c.getTimeInMillis()));
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		
		c.set(Calendar.DAY_OF_MONTH, 2);
		
		t.setDate(new Date(c.getTimeInMillis()));
		tDAO.addObjectAndGetID(t);
		
		c.set(Calendar.YEAR, 2017);
		
		t.setDate(new Date(c.getTimeInMillis()));
		tDAO.addObjectAndGetID(t);
		
		try {
			assertEquals(tDAO.searchByDate(new GregorianCalendar(2000, Calendar.JANUARY, 1), aID).size(), 2);
			assertEquals(tDAO.searchByDate(new GregorianCalendar(2000, Calendar.JANUARY, 2), aID).size(), 1);
			assertEquals(tDAO.searchByDate(new GregorianCalendar(2017, Calendar.JANUARY, 2), aID).size(), 1);
			assertEquals(tDAO.searchByDate(new GregorianCalendar(3000, Calendar.JANUARY, 1), aID).size(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		cleanUp();
	}
	
	@Test
	public void testSearchByMonth() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		Calendar c = new GregorianCalendar();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("lel");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		c.set(Calendar.DAY_OF_MONTH, 31);
		t.setDate(new Date(c.getTimeInMillis()));
		
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		
		c.add(Calendar.DAY_OF_MONTH, 1);
		t.setDate(new Date(c.getTimeInMillis()));
		
		tDAO.addObjectAndGetID(t);
		
		Calendar search = new GregorianCalendar();
		try {
			search.set(Calendar.MONTH, Calendar.OCTOBER);
			assertEquals(tDAO.searchByMonth(search, aID).size(), 2);
			
			search.set(Calendar.MONTH, Calendar.NOVEMBER);
			assertEquals(tDAO.searchByMonth(search, aID).size(), 1);
			
			search.set(Calendar.YEAR, 1000);
			assertEquals(tDAO.searchByMonth(search, aID).size(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cleanUp();
	}
	
	@Test
	public void testSearchByYear() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		Calendar c = new GregorianCalendar();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("kappa123");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		c.set(Calendar.DAY_OF_MONTH, 31);
		c.set(Calendar.MONTH, Calendar.DECEMBER);
		t.setDate(new Date(c.getTimeInMillis()));
		
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		
		c.set(Calendar.YEAR, 2018);
		c.set(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		
		t.setDate(new Date(c.getTimeInMillis()));
		
		tDAO.addObjectAndGetID(t);
		
		Calendar search = new GregorianCalendar();
		try {
			// search for transactions in 2017
			assertEquals(tDAO.searchByYear(search, aID).size(), 2);
			search.add(Calendar.YEAR, 1);
			
			// search for transactions in 2018
			assertEquals(tDAO.searchByYear(search, aID).size(), 1);
			
			search.add(Calendar.YEAR, 1000);
	
			// search for transactions in 3018
			assertEquals(tDAO.searchByYear(search, aID).size(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cleanUp();
	}
	
	@Test
	public void testSearchByWeek() {
		TransactionDAO tDAO = new TransactionDAO();
		Transaction t = new Transaction();
		Calendar c = new GregorianCalendar();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("lol");
		int aID = aDAO.addObjectAndGetID(a);	
		t.setAccountID(aID);
		
		// add transactions for today
		t.setDate(new Date(c.getTimeInMillis()));
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.add(Calendar.WEEK_OF_YEAR, 1);
		t.setDate(new Date(c.getTimeInMillis()));
	
		// add transactions for next monday
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		tDAO.addObjectAndGetID(t);
		
		Calendar search = new GregorianCalendar();
		try {
			// check that there are 2 transactions this week
			assertEquals(tDAO.searchByWeek(search, aID).size(), 2);
			
			search.add(Calendar.WEEK_OF_YEAR, 1);
			// check that there are 3 transactions next week
			assertEquals(tDAO.searchByWeek(search, aID).size(), 3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cleanUp();
	}
	
	@Test
	public void testSearchByCategory() {
		TransactionDAO tDAO = new TransactionDAO();
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("xd");
		int aID = aDAO.addObjectAndGetID(a);	
		
		Transaction t = new Transaction();

		Category c1 = new Category(aID, "c1");
		Category c2 = new Category(aID, "c2a");
		Category c3 = new Category(aID, "c2b");

		t.setAccountID(aID);
		
		int c1ID = cDAO.addObjectAndGetID(c1);
		int c2ID = cDAO.addObjectAndGetID(c2);
		int c3ID = cDAO.addObjectAndGetID(c3);
		
		t.setCategoryID(c1ID);
		tDAO.addObjectAndGetID(t);
		
		t.setCategoryID(c2ID);
		tDAO.addObjectAndGetID(t);
		
		t.setCategoryID(c2ID);
		tDAO.addObjectAndGetID(t);
		
		try {
			assertEquals(tDAO.getByCategory(c1ID).size(), 1);
			assertEquals(tDAO.getByCategory(c2ID).size(), 2);
			assertEquals(tDAO.getByCategory(c3ID).size(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cleanUp();
	}
	
	@Test
	public void testSearchTransactions() {
		TransactionDAO tDAO = new TransactionDAO();
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Account a = new Account();
		a.setUsername("jjj");
		int aID = aDAO.addObjectAndGetID(a);	
		
		Transaction t = new Transaction();
		t.setAccountID(aID);
		
		try {
			int cID = cDAO.addObjectAndGetID("test1", aID);
			
			// add plain transaction
			tDAO.addObjectAndGetID(t);
			
			t.setDescription("xd");
			
			// add transaction with description
			tDAO.addObjectAndGetID(t);
			
			t.setCategoryID(cID);
			
			// add transaction with category
			tDAO.addObjectAndGetID(t);
			
			Calendar c = new GregorianCalendar();
			c.set(Calendar.YEAR, 2000);
			t.setDate(new Date(c.getTimeInMillis()));
			
			// add ancient transaction 
			tDAO.addObjectAndGetID(t);
			
			Calendar today = new GregorianCalendar();
			
			assertEquals(tDAO.searchTransactions("", null, null, -1, aID).size(), 4);
			assertEquals(tDAO.searchTransactions("x", null, null, -1, aID).size(), 3);
			assertEquals(tDAO.searchTransactions("x", null, null, cID, aID).size(), 2);
			assertEquals(tDAO.searchTransactions("", new Date(c.getTimeInMillis()), null, -1, aID).size(), 4);
			assertEquals(tDAO.searchTransactions("", null, new Date(c.getTimeInMillis()), -1, aID).size(), 0);
			assertEquals(tDAO.searchTransactions("", new Date(c.getTimeInMillis()), new Date(today.getTimeInMillis()), cID, aID).size(), 1);
			assertEquals(tDAO.searchTransactions("NOMATCH", null, null, cID, aID).size(), 0);
			assertEquals(tDAO.searchTransactions("", null, new Date(c.getTimeInMillis()), cID, -1).size(), 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public void cleanUp() {
		try {
			DBUtil.clearAllTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
