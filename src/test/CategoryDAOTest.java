package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import dao.AccountDAO;
import dao.CategoryDAO;
import model.Account;
import model.Category;
import util.DBUtil;

public class CategoryDAOTest {

	@BeforeClass
	public static void setUpClass() {
	    //executed only once, before the first test
		//clear table if it exists
		try {
			DBUtil.clearAllTables();
		} catch (Exception e) {
			System.out.println("CategoryTest: clearing CATEGORY table");
		}
		AccountDAO aDAO = new AccountDAO();
		CategoryDAO cDAO = new CategoryDAO();
		aDAO.createTable();
		cDAO.createTable();
	}
	
	@Test
	public void testInsertAndGet() {
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Category c = new Category();
		Account a = new Account();
		
		c.setName("TEST");
		
		// insert account and account into db
		int accountID = aDAO.addObjectAndGetID(a);
		assertEquals(accountID, aDAO.getObject(accountID).getAccountID());
		c.setAccountID(accountID);
		int categoryID = cDAO.addObjectAndGetID(c);
		
		// get inserted category and compare fields
		assertEquals(cDAO.getObject(categoryID).getCategoryID(), categoryID);
		assertEquals(cDAO.getObject(categoryID).getName(), "TEST");
		
		cleanUp();	
	}

	@Test
	public void testUpdate() {
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Category c = new Category();
		Account a = new Account();
		
		int accountID = aDAO.addObjectAndGetID(a);
		assertEquals(accountID, aDAO.getObject(accountID).getAccountID());
		c.setAccountID(accountID);
		int categoryID = cDAO.addObjectAndGetID(c);
				
		c = cDAO.getObject(categoryID);
		c.setName("TEST");

		// update name field
		cDAO.updateObject(c);
		
		// check update success
		assertEquals(cDAO.getObject(categoryID).getName(), "TEST");
		
		cleanUp();	
	}
	
	@Test
	public void testGetAll() {
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Category c = new Category();
		Account a = new Account();		
		final int n = 10;
		
		int accountID = aDAO.addObjectAndGetID(a);
		assertEquals(accountID, aDAO.getObject(accountID).getAccountID());
		c.setAccountID(accountID);

		for (int i = 0; i < n; i++) {
			cDAO.addObjectAndGetID(c);
		}
		
		List<Category> l = cDAO.getAllObjects();
		assertTrue(l.size() == n);
		
		cleanUp();
	}
		
	@Test
	public void testDeleteByID() {
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Category c = new Category();
		Account a = new Account();		
		final int n = 10;

		int accountID = aDAO.addObjectAndGetID(a);
		assertEquals(accountID, aDAO.getObject(accountID).getAccountID());
		c.setAccountID(accountID);

		for (int i = 0; i < n; i++) {
			cDAO.addObjectAndGetID(c);
		}
		
		List<Category> l = cDAO.getAllObjects();
		assertTrue(l.size() == n);
		
		// delete objects that were added
		for (int i = 0; i < n; i++) {
			cDAO.deleteObject(n-i);
			l = cDAO.getAllObjects();
			assertEquals(l.size(), n-i-1);
		}
		
		cleanUp();
	}
	
	@Test
	public void testDeleteByObject() {
		CategoryDAO cDAO = new CategoryDAO();
		AccountDAO aDAO = new AccountDAO();
		Category c = new Category();
		Account a = new Account();			
		final int n = 10;

		int accountID = aDAO.addObjectAndGetID(a);
		assertEquals(accountID, aDAO.getObject(accountID).getAccountID());
		c.setAccountID(accountID);
		
		try {
			for (int i = 0; i < n; i++) {
				cDAO.addObjectAndGetID(c);
			}
			
			List<Category> l = cDAO.getAllObjects();
			assertTrue(l.size() == n);
			
			// delete objects that were added
			for (int i = 0; i < n; i++) {
				cDAO.deleteObject(l.get(i));
				List<Category> l2 = cDAO.getAllObjects();
				assertEquals(l2.size(), n-i-1);
			}
			
			cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cleanUp() {
		try {
			DBUtil.clearTable("ACCOUNT");
			DBUtil.clearTable("CATEGORY");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
