package dao;

import java.sql.ResultSet;
import javafx.collections.ObservableList;
import model.Category;
import util.DBUtil;

public class AccountCategoryDAO {
	/**
	 * Returns all categories belonging to accountID
	 * @param accountID account id
	 * @return A list of categories belonging to the accountID
	 */
	public ObservableList<Category> getCategories(int accountID) {
		String selectStmt = "SELECT *"
				+ " FROM CATEGORY"
				+ " WHERE ACCOUNT_ID =" + accountID;
		return getCategories(selectStmt);
	}
	
	/**
	 * Returns all categories belonging to accountID and matching searchStr
	 * @param accountID account id
	 * @param searchStr string to search in category names
	 * @return A list of categories matching the accountID and searchStr
	 */
	public ObservableList<Category> getCategories(int accountID, String searchStr) {
		String selectStmt = "SELECT *"
				+ " FROM CATEGORY"
				+ " WHERE ACCOUNT_ID =" + accountID
				+ " AND UPPER(NAME) LIKE UPPER('%"+searchStr+"%')";
		
		return getCategories(selectStmt);
	}
	
	/**
	 * Queries category table
	 * @param selectStmt query to execute
	 * @return list of categories matching the query
	 */
	private ObservableList<Category> getCategories(String selectStmt) {
		try {
			CategoryDAO cDAO = new CategoryDAO();
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// extract category fields
			return cDAO.getFromResultSet(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
}