package dao;

import java.sql.Date;
import java.sql.SQLException;

import dao.TransactionDAO.Period;
import javafx.collections.ObservableList;
import model.Category;
import model.ExpenseCategory;
import model.Limit;
import model.Transaction;

/**
 * Class that brings together the useful functions from Transaction, Category and Limit DAO classes
 * @author bing
 */
public class Budget {
	private AccountCategoryDAO acDAO;
	private ExpenseCategoryDAO ecDAO;
	private CategoryDAO cDAO;
	private LimitDAO lDAO;
	private TransactionDAO tDAO;
	
	public Budget() {
		this.acDAO = new AccountCategoryDAO();
		this.cDAO = new CategoryDAO();
		this.ecDAO = new ExpenseCategoryDAO();
		this.lDAO = new LimitDAO();
		this.tDAO = new TransactionDAO();
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Add Functions
	///////////////////////////////////////////////////////////////////////////////

	/**
	 * @param categoryName Name of the category to add
	 * @param accountID ID of the account that the category belongs to
	 * @return The ID of the row the category was inserted into
	 * @throws Exception
	 */
	public int addIncomeCategory(String categoryName, int accountID) throws Exception {
		return cDAO.addObjectAndGetID(categoryName, accountID);
	}
	
	/**
	 * Inserts an expenseCategory into the DB given a category name and accountID
	 * @param categoryName The name of the category
	 * @param maxAmount Maximum amount for the category
	 * @param accountID The account that the category belongs to
	 * @param period The period that the spending limit applies for
	 */
	public void addExpenseCategory(String categoryName, double maxAmount, int accountID, Period period) throws Exception {
		ecDAO.addExpenseCategory(categoryName, maxAmount, accountID, period);
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Delete Functions
	///////////////////////////////////////////////////////////////////////////////
	/**
	 * Deletes a category given a categoryID
	 * @param categoryID The ID of the category to delete
	 */
	public void deleteCategory(int categoryID) {
		cDAO.deleteObject(categoryID);
	}
	
	/**
	 * Deletes an entry from the category and limit DB given a categoryID
	 * @param categoryID ID of the category to delete
	 */
	public void deleteExpenseCategory(int categoryID) {
		ecDAO.deleteExpenseCategory(categoryID);
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Update Query Functions
	///////////////////////////////////////////////////////////////////////////////
	/**
	 * Updates a category in the DB with a matching categoryID in category
	 * @param category Category to update
	 */
	public void updateCategory(Category category) {
		cDAO.updateObject(category);
	}
	
	/**
	 * Updates a limit in the DB with a matching limitID in limit
	 * @param limit Limit to update
	 */
	public void updateLimit(Limit limit) {
		lDAO.updateObject(limit);
	}
		
	///////////////////////////////////////////////////////////////////////////////
	// Transaction Search Functions
	///////////////////////////////////////////////////////////////////////////////
	/**
	 * Searches for transactions that matches the description, date range and/or category
	 * @param searchStr The string the user wants to search for in description
	 * @param begin Inclusive start date to search for transactions OR null if no input
	 * @param end Exclusive end date to search for transactions OR null if no input
	 * @param categoryID ID of the category OR -1 if no user input entered
	 * @param accountID ID of the account
	 * @return A list of transactions matching the search inputs
	 */
	public ObservableList<Transaction> searchTransactions(String searchStr, Date begin, Date end, int categoryID, int accountID) throws Exception {
		return tDAO.searchTransactions(searchStr, begin, end, categoryID, accountID);
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Limit Search Functions
	///////////////////////////////////////////////////////////////////////////////
	/**
	 * Get a limit by categoryID
	 * @param categoryID ID of the category
	 * @return A limit that contains a matching categoryID
	 */
	public Limit getLimit(int categoryID) {
		 return lDAO.getObjectWithCategoryID(categoryID);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	// Category Search Functions
	///////////////////////////////////////////////////////////////////////////////
	/**
	 * Retrieves all categories belonging to the accountID
	 * @param accountID ID of the account
	 * @return A list of categories that matches the accountID
	 */
	public ObservableList<Category> getAllCategories(int accountID) {	
		return acDAO.getCategories(accountID);
	}
	
	/**
	 * Retrieves categories that match searchStr and belong to accountID
	 * @param accountID ID of the account
	 * @param searchStr String to find when searching category names
	 * @return A list of categories that matches the accountID
	 */
	public ObservableList<Category> searchAllCategories(int accountID, String searchStr) {	
		return acDAO.getCategories(accountID, searchStr);
	}
	
	/**
	 * Gets all income categories for the given accountID
	 * @param accountID ID of the account
	 * @return A list of categories belonging to the account
	 */
	public ObservableList<Category> getIncomeCategories(int accountID) {	
		return cDAO.getIncomeCategories(accountID);
	}
	
	/**
	 * Searches for categories that match the given name and accountID
	 * @param categoryName Name to search for in categories
	 * @param accountID ID of the account to search
	 * @return List of categories that match the name and accountID
	 */
	public ObservableList<Category> searchIncomeCategories(String categoryName, int accountID) {
		return cDAO.getObjectByName(categoryName, accountID);
	}
	
	public ObservableList<Category> searchCategoriesExact(String categoryName, int accountID) {
		return cDAO.getByExactName(categoryName, accountID);
	}
	
	/**
	 * Gets the account's expense categories
	 * @param accountID ID of the account
	 * @return List of acocunt's expense categories
	 */
	public ObservableList<ExpenseCategory> getAllExpenseCategories(int accountID) {
		return this.ecDAO.getAllExpenseCategories(accountID);
	}
	
	/**
	 * Gets the account's expense categories that have category names matching searchStr
	 * @param accountID ID of the account
	 * @param searchStr String to search for when comparing category names
	 * @return List of expense categories with names matching searchStr
	 */
	public ObservableList<ExpenseCategory> searchExpenseCategories(int accountID, String searchStr) {
		return this.ecDAO.getExpenseCategories(accountID, searchStr);
	}
	
	public boolean isExpenseCategory(int categoryID) throws SQLException, ClassNotFoundException {
		return this.ecDAO.isExpenseCategory(categoryID);
	}
}