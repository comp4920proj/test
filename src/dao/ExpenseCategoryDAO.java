package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import dao.TransactionDAO.Period;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Category;
import model.ExpenseCategory;
import model.Limit;
import util.DBUtil;

/**
 * Accesses CATEGORY and LIMIT tables together to form an expense category
 * @author bing
 */
// TODO: delete function
public class ExpenseCategoryDAO {
	private CategoryDAO cDAO;
	private LimitDAO lDAO;
	
	public ExpenseCategoryDAO() {
		this.cDAO = new CategoryDAO();
		this.lDAO = new LimitDAO();
	}
	
	/**
	 * Add expense category but specify a if it's editable
	 * @param categoryName The name of the category
	 * @param maxAmount Maximum amount for the category
	 * @param accountID The account that the category belongs to
	 * @param period The period that the spending limit applies for	 
	 * @param isDeletable If the category is deletable
	 */
	public void addExpenseCategory(String categoryName, double maxAmount, int accountID, Period period, boolean isDeletable) {
    	Category c = new Category(accountID, categoryName, isDeletable);
    	int categoryID = cDAO.addObjectAndGetID(c);
    	
    	Limit l = new Limit(categoryID, maxAmount);
    	l.setPeriod(period);
    	lDAO.addObject(l);
	}
	
	/**
	 * Inserts an expenseCategory into the DB given a category name and accountID and it's deletable
	 * @param categoryName The name of the category
	 * @param maxAmount Maximum amount for the category
	 * @param accountID The account that the category belongs to
	 * @param period The period that the spending limit applies for
	 */
	public void addExpenseCategory(String categoryName, double maxAmount, int accountID, Period period) {
    	this.addExpenseCategory(categoryName, maxAmount, accountID, period, true);
	}
	
	/**
	 * Inserts an expenseCategory into the DB given a category name and accountID
	 * @param categoryName The name of the category
	 * @param maxAmount Maximum amount for the category
	 * @param accountID The account that the category belongs to
	 */
	public void addExpenseCategory(String categoryName, double maxAmount, int accountID) {    	
    	this.addExpenseCategory(categoryName, maxAmount, accountID, Period.WEEKLY);
	}

	/**
	 * Inserts an expenseCategory given a category name, accountID and if it's deletable 
	 * @param categoryName The name of the category
	 * @param maxAmount Maximum amount for the category
	 * @param accountID The account that the category belongs to
	 */
	public void addExpenseCategory(String categoryName, double maxAmount, int accountID, boolean isDeletable) {    	
    	this.addExpenseCategory(categoryName, maxAmount, accountID, Period.WEEKLY, false);
	}
	
	/**
	 * Deletes an entry from category and limit table with a categoryID
	 * @param categoryID The categoryID of the category and limit to delete
	 */
	public void deleteExpenseCategory(int categoryID) {
		lDAO.deleteObject(categoryID);
		cDAO.deleteObject(categoryID);
	}
	
	/**
	 * Deletes an entry from the category table and its limit in the limit table
	 * @param ec The expense category to delete
	 */
	public void deleteExpenseCategory(ExpenseCategory ec) {
		lDAO.deleteObject(ec.getLimit());
		cDAO.deleteObject(ec.getCategoryID());
	}
	
	public boolean isExpenseCategory(int categoryID) throws SQLException, ClassNotFoundException {
		String selectStmt = "SELECT COUNT(*) AS COUNT"
				+ " FROM LIMIT"
				+ " WHERE CATEGORY_ID=" + categoryID;
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// if there is more than 1 row matching above query
			// there is a record in limit table that matches categoryID
			if (rs.next() && rs.getInt("COUNT") > 0) {
				return true;
			}
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}

	/**
	 * Get expense categories matching accountID and searchStr
	 * @param accountID account id
	 * @param searchStr string to search in category's name
	 * @return list of categories matching accountID and searchStr
	 */
	public ObservableList<ExpenseCategory> getExpenseCategories(int accountID, String searchStr) {
		String selectStmt = "SELECT *"
				+ " FROM CATEGORY, LIMIT"
				+ " WHERE UPPER(NAME) LIKE UPPER('%"+searchStr+"%')"
				+ " AND LIMIT.CATEGORY_ID=CATEGORY.CATEGORY_ID"
				+ " AND CATEGORY.ACCOUNT_ID="+accountID+"";
		
		return this.getCategories(selectStmt);		
	}
	
	/**
	 * Get expense categories matching accountID
	 * @param accountID account id
	 * @param searchStr string to search in category's name
	 * @return list of categories matching accountID and searchStr
	 */
	public ObservableList<ExpenseCategory> getAllExpenseCategories(int accountID) {
		String selectStmt = "SELECT *"
				+ " FROM CATEGORY, LIMIT"
				+ " WHERE LIMIT.CATEGORY_ID=CATEGORY.CATEGORY_ID"
				+ " AND CATEGORY.ACCOUNT_ID="+accountID+"";
		
		return this.getCategories(selectStmt);		
	}
	
	/**
	 * Queries category and limit table to form a list of expense categories
	 * @param selectStmt query to execute
	 * @return list of categories matching the selectStmt, null if no categories exist
	 */
	public ObservableList<ExpenseCategory> getCategories(String selectStmt) {		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			ObservableList<ExpenseCategory> categoryList = FXCollections.observableArrayList();
			
			ObservableList<Category> cList = cDAO.getFromResultSet(rs); 
			ObservableList<Limit> lList = FXCollections.observableArrayList();
			
			// reset resultSet point to first row and populate limit list
			rs.beforeFirst();
			lList = lDAO.getFromResultSet(rs);
			
			// both lists should always have same size
			if (cList.size() == lList.size()) {
				for (int i = 0; i < cList.size(); i++) {
					ExpenseCategory category = new ExpenseCategory();

					category.setCategoryID(cList.get(i).getCategoryID());
					category.setAccountID(cList.get(i).getAccountID());
					category.setName(cList.get(i).getName());
					category.setLimit(lList.get(i));
					// calculate current amount / progress
					category.getLimit().calculateAndSetCurrentAmount();
					categoryList.add(category);
				}
			} 			
			
			return categoryList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}