package dao;

import java.util.List;

public interface ObjectDAO<E> {
	public List<E> getAllObjects(); 
	public E getObject(int id);
	public void updateObject(E Object);
	public void deleteObject(E Object);
	public void createTable();
}