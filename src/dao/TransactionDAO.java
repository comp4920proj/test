package dao;
import model.Account;
import model.Category;
import model.Transaction;
import util.DBUtil;
import util.CalendarPair;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Date;

public class TransactionDAO implements ObjectDAO<Transaction> {
	public enum Period { DAILY, WEEKLY, MONTHLY, YEARLY, OTHER };
	
	/**
	 * Grabs all transactions, whether they belong to the account or not.
	 * Use getAccountTransactions()
	 */
	@Deprecated
	public ObservableList<Transaction> getAllObjects() {
		String selectStmt = "SELECT * FROM EVENT";
		ObservableList<Transaction> transactionList = FXCollections.observableArrayList();

		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			transactionList = getFromResultSet(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionList;
	}
	
	/**
	 * Searches for transactions that matches the description, date range and/or category. 
	 * If user only enters a startDate, it will search for transactions on and after the date.
	 * If user only enters an endDate, it will search for transactions before the date.
	 * @param searchStr The string the user wants to search for in description
	 * @param begin Inclusive start date to search for transactions OR null if no input
	 * @param end Inclusive end date to search for transactions OR null if no input
	 * @param categoryID ID of the category OR -1 if no user input entered
	 * @param accountID ID of the account. MUST BE SUPPLIED.
	 * @return A list of transactions matching the search inputs
	 */
	public ObservableList<Transaction> searchTransactions(String searchStr, Date begin, Date end, int categoryID, int accountID) throws Exception {
		String startDate = (begin == null) ? "" : " AND DAY >='"+begin+"'";
		String endDate = (end == null) ? "" : " AND DAY <='"+end+"'";
		String categoryCondition = (categoryID == -1) ? "" : " AND CATEGORY_ID="+categoryID;
		
		String selectStmt = "SELECT *"
				+ " FROM EVENT"
				+ " WHERE UPPER(DESCRIPTION) LIKE UPPER('%"+searchStr+"%')"
				+ " AND ACCOUNT_ID="+accountID
				+ startDate
				+ endDate
				+ categoryCondition;
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			ObservableList<Transaction> list = getFromResultSet(rs);
			return list;
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Grabs all transactions belonging to an account
	 * @param accountID The ID of the account to get transactions for
	 * @return A list of transactions belonging to the account
	 */
	public ObservableList<Transaction> getAccountTransactions(int accountID) {
		String selectStmt = "SELECT * FROM EVENT WHERE ACCOUNT_ID=" + accountID;
		ObservableList<Transaction> transactionList = FXCollections.observableArrayList();

		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			transactionList = getFromResultSet(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return transactionList;		
	}

	/**
	 * Gets all favourite transactions that belong to the account
	 * @param accountID ID of the account to get favourite transactions for
	 * @return A list of favourite transactions belonging to the account
	 * @throws Exception
	 */
	public ObservableList<Transaction> getFavouriteTransactions(int accountID) throws Exception {
		String selectStmt = "SELECT *"
						+ " FROM EVENT"
						+ " WHERE ACCOUNT_ID="+accountID+"" 
						+ " AND FAVOURITE=TRUE";
		
		ObservableList<Transaction> transactionList = FXCollections.observableArrayList();
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			transactionList = getFromResultSet(rs);
		} catch (Exception e) {
			throw new Exception("Database Error (getFavouriteTransactions)", e);
		}
		return transactionList;
	}
	
	/**
	 * Grabs a transaction with a specific transactionID
	 */
	public Transaction getObject(int transactionID) {
		String selectStmt = "SELECT * FROM EVENT WHERE EVENT_ID=" + transactionID;

		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// Extract fields and put into transaction object
			List<Transaction> transactionList = this.getFromResultSet(rs);
			if (transactionList.size() > 0) {
				System.out.println("RETRIEVING CATEGORY: " + transactionList.get(0).getCategoryName());
				return transactionList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets transactions belonging to a given categoryID
	 * @param categoryID The category ID of the transactions to get
	 * @return A list of transactions that contain the categoryID
	 * @throws Exception
	 */
	public ObservableList<Transaction> getByCategory(int categoryID) throws Exception {
		// since all categories belong to a certain account
		// we don't need to use accountID to filter transactions
		String selectStmt = "SELECT * FROM EVENT, CATEGORY"
				+ " WHERE CATEGORY.CATEGORY_ID = "+categoryID+""
				+ " AND EVENT.CATEGORY_ID=CATEGORY.CATEGORY_ID";
				
		try {
			ObservableList<Transaction> list = this.getTransaction(selectStmt);
			return list;
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/**
	 * Overloaded getByCategory(int categoryID)
	 * @param category The category of the transactions to get
	 * @return A list of transactions that belong to the category
	 * @throws Exception
	 */
	public ObservableList<Transaction> getByCategory(Category category) throws Exception {
		try {
			ObservableList<Transaction> list = this.getByCategory(category.getCategoryID());
			return list;
		} catch (Exception e) {
			throw e;
		}		
	}
	
	////////////////////////////////////////////////////////////////////
	// Search by date functions
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Search for transactions within a date range and category
	 * @param category Category to search for
	 * @param date Date to search around
	 * @param period DAILY, WEEKLY, MONTHLY or YEARLY
	 * @return A list of transactions within the date and category
	 */
	public ObservableList<Transaction> searchByCategoryAndDate(Category category, Calendar date, Period period) throws Exception {
		return this.searchByCategoryAndDate(category.getCategoryID(), date, period);
	}
	
	/**
	 * Search for transactions within a date range and category
	 * @param categoryID ID of the category to search for
	 * @param date Date to search around
	 * @param period DAILY, WEEKLY, MONTHLY or YEARLY
	 * @return A list of transactions within the date and category
	 */
	public ObservableList<Transaction> searchByCategoryAndDate(int categoryID, Calendar date, Period period) throws Exception {
		ObservableList<Transaction> transactionList = FXCollections.observableArrayList();
		CalendarPair dateRange = this.getStartEndDate(date, period);
		Date begin = new Date(dateRange.getStart().getTimeInMillis());
		Date end = new Date(dateRange.getEnd().getTimeInMillis());
		
		String selectStmt = "SELECT *"
				+ " FROM EVENT, CATEGORY"
				+ " WHERE DAY >= '"+begin+"'"
				+ " AND   DAY < '"+end+"'"
				+ " AND   CATEGORY.CATEGORY_ID = "+categoryID+""
				+ " AND   EVENT.CATEGORY_ID = CATEGORY.CATEGORY_ID";	
		
		try {
			transactionList = this.getTransaction(selectStmt);
		} catch (Exception e) {
			throw e;
		}
		
		return transactionList;
	}
	
	/**
	 * Searches by date range [begin, end)
	 * @param begin Beginning date
	 * @param end End date
	 * @return List of transactions that occurred on/after begin and before end
	 */
	public ObservableList<Transaction> searchByDateRange(Date begin, Date end, int accountID) throws Exception {
		String selectStmt = "SELECT *"
				+ " FROM EVENT"
				+ " WHERE DAY >= '"+begin+"'"
				+ " AND   DAY < '"+end+"'"
				+ " AND   ACCOUNT_ID =" + accountID;
		try {
			return this.getTransaction(selectStmt);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Provides a start and end date for searchByDate functions
	 * @param c Date to provide a range for
	 * @param period Daily, weekly, monthly or yearly
	 * @return A start and end date for the given date and period
	 */
	public CalendarPair getStartEndDate(Calendar cal, Period period) {
		Calendar c = (Calendar) cal.clone();
		CalendarPair cp = new CalendarPair();
		Calendar start = new GregorianCalendar();
		Calendar end = new GregorianCalendar(); 
		
		switch(period) {
			case DAILY:
				start = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				end = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				
				// get next day
				end.add(Calendar.DAY_OF_MONTH, 1);
				break;
			case WEEKLY:
				// set calendar date to current week's monday
				// need to set first day of week as default is sunday
				c.setFirstDayOfWeek(Calendar.MONDAY);
				c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

				start = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				end = new GregorianCalendar(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));
				
				// get next monday
				end.add(Calendar.WEEK_OF_YEAR, 1);
				break;
			case MONTHLY:
				start = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), 1);
				end = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), 1);
				
				// get next month
				end.add(Calendar.MONTH, 1);
				break;
			case YEARLY:
				start = new GregorianCalendar(c.get(Calendar.YEAR), Calendar.JANUARY, 1);
				end = new GregorianCalendar(c.get(Calendar.YEAR), Calendar.JANUARY, 1);
				
				// get next year
				end.add(Calendar.YEAR, 1);
				break;
			default:
				start = new GregorianCalendar(0, Calendar.JANUARY, 1);
				end = new GregorianCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				break;
		}
		
		cp.setStart(start);
		cp.setEnd(end);
		
		return cp;
	}
	
	public ObservableList<Transaction> searchByDateRange(Calendar begin, Calendar end, int accountID) throws Exception {
		try {
			return this.searchByDateRange(new Date(begin.getTimeInMillis()), new Date(end.getTimeInMillis()), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Search for transactions on a specific day
	 * @param c Calendar object
	 * @return list of transactions matching the day
	 */
	public ObservableList<Transaction> searchByDate(Calendar c, int accountID) throws Exception {
		// get today's date and tomorrow
		CalendarPair cp = this.getStartEndDate(c, Period.DAILY);
		
		try {
			return this.searchByDateRange(cp.getStart(), cp.getEnd(), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Overloaded searchByDate function that uses a Calendar argument
	 * @param day java.sql.Date
	 * @return list of transactions matching the day
	 */
	public ObservableList<Transaction> searchByDate(Date day, int accountID) throws Exception {
		Calendar c = new GregorianCalendar();
		c.setTime(day);
		try {
			return this.searchByDate(c, accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Search for transactions within the week (current Monday-Sunday)
	 * @param c the date that gives the week to search
	 * @return list of transactions within the week
	 */
	public ObservableList<Transaction> searchByWeek(Calendar c, int accountID) throws Exception {
		// get this week's monday and the next monday
		CalendarPair cp = this.getStartEndDate(c, Period.WEEKLY);
		
		try {
			return this.searchByDateRange(cp.getStart(), cp.getEnd(), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Searches transactions in a specific month and year
	 * @param c Calendar containing a month and year to filter transactions
	 * @return list of transactions matching month and year in c
	 */
	public ObservableList<Transaction> searchByMonth(Calendar c, int accountID) throws Exception {		
		// get start of this and next month
		CalendarPair cp = this.getStartEndDate(c, Period.MONTHLY);
		
		try {
			return this.searchByDateRange(cp.getStart(), cp.getEnd(), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Searches transactions in a specific year
	 * @param c Calendar containing a year to filter transactions
	 * @return list of transactions matching year in c
	 */
	public ObservableList<Transaction> searchByYear(Calendar c, int accountID) throws Exception {
		// get start of this and next month
		CalendarPair cp = this.getStartEndDate(c, Period.YEARLY);
		
		try {
			return this.searchByDateRange(cp.getStart(), cp.getEnd(), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Gets all transactions before the given date
	 * @param c The date
	 * @param accountID The ID of the account
	 * @return A list of transactions belonging to the accountID that occur on and/or before the date
	 * @throws Exception
	 */
	public ObservableList<Transaction> searchAllBeforeDate(Calendar c, int accountID) throws Exception {
		// get start of this and next month
		CalendarPair cp = this.getStartEndDate(c, Period.OTHER);
		
		try {
			return this.searchByDateRange(cp.getStart(), cp.getEnd(), accountID);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Queries transactions with a sql statement
	 * @param selectStmt query to run on the transaction table
	 * @return a list of transactions matching the selectStmt
	 */
	public ObservableList<Transaction> getTransaction(String selectStmt) throws Exception {
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			ObservableList<Transaction> transactionList = this.getFromResultSet(rs);
			return transactionList;
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Converts a resultSet into a list of Transactions
	 * @param rs resultSet to convert
	 * @return a list of Transactions from the resultSet
	 * @throws SQLException
	 */
	private ObservableList<Transaction> getFromResultSet(ResultSet rs) throws SQLException  {
		ObservableList<Transaction> transactionList = FXCollections.observableArrayList();
		try {
			while (rs.next()) {
				Transaction transaction = new Transaction();
				transaction.setTransactionID(rs.getInt("EVENT_ID"));
				transaction.setAccountID(rs.getInt("ACCOUNT_ID"));
				transaction.setCategoryID(rs.getInt("CATEGORY_ID"));
				transaction.setAmount(rs.getFloat("AMOUNT"));
				transaction.setDate(rs.getDate("DAY"));
				transaction.setDescription(rs.getString("DESCRIPTION"));
				transaction.setFavourite(rs.getBoolean("FAVOURITE"));
				transaction.setIsExpense(rs.getBoolean("IS_EXPENSE"));
				transaction.setPriority(rs.getInt("PRIORITY"));
				transactionList.add(transaction);
			}
		} catch (Exception e) {
			throw e;
		}
		return transactionList;
	}

	/**
	 * Edit a transaction in the database.
	 * Will never update ACCOUNT_ID or EVENT_ID.
	 * @param o Transaction to update
	 */
	public void updateObject(Transaction o) {
		String categoryID = (o.getCategoryID() > 0) ? ""+o.getCategoryID()+"" : "null";
		String updateStmt = "UPDATE EVENT" +
				" SET    AMOUNT = "+o.getAmount()+"," + 
				"        DAY = '"+o.getDate()+"'," +
				"        DESCRIPTION = '"+o.getDescription()+"'," +
				"        FAVOURITE = "+o.isFavourite()+"," +
				"        CATEGORY_ID = "+categoryID+","+
				"        PRIORITY = "+o.getPriority()+"," +
				"        IS_EXPENSE = "+o.isExpense()+"" +
				" WHERE  EVENT_ID = "+o.getTransactionID()+"";		
		try {
			DBUtil.executeUpdateDB(updateStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void editDescription(Transaction t, String newDescription) {
		t.setDescription(newDescription);
		updateObject(t);
	}

	public void editAmount(Transaction t, double newAmount) {
		t.setAmount(newAmount);
		System.out.println("new amount and trans id= " + newAmount + " id = " + t.getTransactionID() );
		updateObject(t);
	}

	public void editFavourite(Transaction t, Boolean favourite) {
		t.setFavourite(favourite);
		updateObject(t);
	}
	
	public void editCategory(Transaction t, Category category) {
		editCategory(t, category.getCategoryID());
	}
	
	public void editCategory(Transaction t, int categoryID) {
		t.setCategoryID(categoryID);
		updateObject(t);
	}

	public void deleteObject(Transaction o) {
		int id = o.getTransactionID();
		String selectStmt = "DELETE FROM EVENT WHERE EVENT_ID=" + id;
		try {
			DBUtil.executeUpdateDB(selectStmt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void removeObject(Transaction t) {	
		String deleteStmt = "SELECT * FROM EVENT WHERE EVENT_ID=" + t.getTransactionID();
		
		try {
			DBUtil.executeUpdateDB(deleteStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Gives a transaction an accountID from an account and inserts in DB
	 * @param o transaction
	 * @param a account
	 */
	public void addObject(Transaction o, Account a) {
		o.setAccountID(a.getAccountID());
		this.addObject(o);
	}
	
	/**
	 * Use addObjectAndGetID or the overloaded addObject instead
	 * @param o Transaction to insert
	 */
	@Deprecated
	public void addObject(Transaction o) {
		String sqlStmt = "INSERT INTO EVENT" +
				" (AMOUNT, DAY, DESCRIPTION, FAVOURITE, IS_EXPENSE, ACCOUNT_ID)" + 
				" VALUES("+o.getAmount()+", '"+o.getDate()+"', '"+o.getDescription()+"', "+o.isFavourite()+", "+o.isExpense()+", "+o.getAccountID()+"" + ")";

		try {
			// Grab id of new rows inserted in the table
			DBUtil.insertAndGetID(sqlStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Inserts a transaction and returns the ID of the row inserted
	 * @param o Transaction to insert
	 * @return ID of inserted transaction in DB
	 */
	public int addObjectAndGetID(Transaction o) {
		// insert NULL for category ID column if no valid category ID given
		String categoryID = (o.getCategoryID() > 0) ? ""+o.getCategoryID()+"" : "null";
		String sqlStmt = "INSERT INTO EVENT" +
				" (AMOUNT, DAY, DESCRIPTION, FAVOURITE, IS_EXPENSE, ACCOUNT_ID, CATEGORY_ID, PRIORITY)" + 
				" VALUES("+o.getAmount()+", '"+o.getDate()+"', '"+o.getDescription()+"', "+o.isFavourite()+", "+o.isExpense()+", "+o.getAccountID()+", " + categoryID +	", "+o.getPriority()+""	+		
				" )";

		try {
			// Grab id of new rows inserted in the table
			ArrayList<Integer> insertedIDs = DBUtil.insertAndGetID(sqlStmt);
			
			if (insertedIDs.size() > 0) {
				return insertedIDs.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void createTable() {
		// Cannot name the table TRANSACTION as it's a reserved word for the database
		String sqlStmt = "create table EVENT (" +
				" EVENT_ID INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
				" AMOUNT FLOAT," +
				" DAY DATE," +
				" DESCRIPTION LONG VARCHAR," +
				" FAVOURITE BOOLEAN," +
				" IS_EXPENSE BOOLEAN," +
				" PRIORITY INTEGER," +
				" ACCOUNT_ID INT CONSTRAINT TRANSACTION_ACCOUNT_FK" +
				" 	REFERENCES ACCOUNT ON DELETE CASCADE ON UPDATE RESTRICT," +
				" CATEGORY_ID INT CONSTRAINT TRANSACTION_CATEGORY_FK" +
				" 	REFERENCES CATEGORY ON DELETE CASCADE ON UPDATE RESTRICT" +
				")"; 

		try {
			DBUtil.createTable(sqlStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}