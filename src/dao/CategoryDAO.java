package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Category;
import util.DBUtil;

public class CategoryDAO implements ObjectDAO<Category> {
	/**
	 * Gets all categories in the category table
	 */
	public ObservableList<Category> getAllObjects() {
		String selectStmt = "SELECT * FROM CATEGORY";
		ObservableList<Category> categoryList = FXCollections.observableArrayList();
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			categoryList = this.getFromResultSet(rs);
		} catch (Exception e) {
			//throw e;
		}
		return categoryList;
	}
	
	/**
	 * Get a category by category id
	 */
	public Category getObject(int id) {
		return getObject("SELECT * FROM CATEGORY WHERE CATEGORY_ID=" + id);
	}
	
	/**
	 * Get categories by name and accountID
	 * @param categoryName string to search in category name
	 * @param accountID ID of the account
	 * @return list of categories with names matching categoryName
	 */
	public ObservableList<Category> getObjectByName(String categoryName, int accountID) {
		return getObjectList("SELECT *"
				+ " FROM CATEGORY"
				+ " WHERE UPPER(NAME) LIKE UPPER('%"+categoryName+"%')"
				+ " AND ACCOUNT_ID="+accountID);
	}
	
	public ObservableList<Category> getByExactName(String categoryName, int accountID) {
		return getObjectList("SELECT *"
				+ " FROM CATEGORY"
				+ " WHERE UPPER(NAME) LIKE UPPER('"+categoryName+"')"
				+ " AND ACCOUNT_ID="+accountID);		
	}
	
	/**
	 * Executes a select query on category table
	 * @param selectStmt query to execute
	 * @return a category matching the query
	 */
	private Category getObject(String selectStmt) {
		ObservableList<Category> categoryList = this.getObjectList(selectStmt);
		
		if (categoryList != null && categoryList.size() > 0) {
			return categoryList.get(0);	
		}
		return null;
	}

	/**
	 * Returns all income categories
	 * @param accountID The account ID to get income categories for
	 * @return A list of income categories
	 */
	public ObservableList<Category> getIncomeCategories(int accountID) {	
		String selectStmt = "SELECT *"
			+ " FROM CATEGORY"
			+ " WHERE CATEGORY_ID NOT IN (SELECT CATEGORY_ID FROM LIMIT WHERE ACCOUNT_ID = "+accountID+")"
			+ " AND ACCOUNT_ID="+accountID+"";
		
		return this.getObjectList(selectStmt);
	}
	
	/**
	 * Executes a select query on category table
	 * @param selectStmt query to execute
	 * @return a list of categories matching the query
	 */
	private ObservableList<Category> getObjectList(String selectStmt)  {
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// Extract fields and put into category object
			ObservableList<Category> categoryList = this.getFromResultSet(rs);
								
			return categoryList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Update category
	 * Will never update accountID
	 */
	public void updateObject(Category Object) {
		String updateStmt = "UPDATE CATEGORY" +
				" SET    NAME = '"+Object.getName()+"'" + 
				" WHERE  CATEGORY_ID = "+Object.getCategoryID()+"";
		try {
			DBUtil.executeUpdateDB(updateStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Deletes a category from category DB
	 */
	public void deleteObject(Category Object) {
		try {
			deleteObject(Object.getCategoryID());
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Deletes a category from category table by its ID
	 * @param id The category id of the category to delete
	 */
	public void deleteObject(int id) {
		String deleteStmt = "DELETE FROM CATEGORY"
				+ " WHERE CATEGORY_ID = " + id;
		
		try {
			DBUtil.executeUpdateDB(deleteStmt);
		} catch (Exception e) {
			//throw e;
		}
	}
		
	/**
	 * Inserts a category into the database that's deletable
	 * @param categoryName The name of the category to insert
	 * @param accountID The account ID of the account that owns the category
	 * @return The ID of the row that the category was inserted in
	 */
	public int addObjectAndGetID(String categoryName, int accountID) throws Exception {
		return addObjectAndGetID(categoryName, accountID, true);
	}
	
	/**
	 * Inserts a category into the database given a bool telling if it's deletable
	 * @param categoryName The name of the category to insert
	 * @param accountID The account ID of the account that owns the category
	 * @return The ID of the row that the category was inserted in
	 */
	public int addObjectAndGetID(String categoryName, int accountID, boolean isDeletable) throws Exception {
		Category category = new Category();
		category.setName(categoryName);
		category.setAccountID(accountID);
		category.setIsDeletable(isDeletable);
		
		return addObjectAndGetID(category);
	}
	
	/**
	 * Inserts a category into the database
	 * @param category category to insert
	 * @param account account that the category belongs to
	 * @return CATEGORY_ID of the newly inserted category
	 */
	public int addObjectAndGetID(Category category) {
		String selectStmt = "INSERT INTO CATEGORY" +
				" (NAME, ACCOUNT_ID, IS_DELETABLE)" + 
				" VALUES('"+category.getName()+"', "+category.getAccountID()+", "+category.getIsDeletable()+")";
		
		return addObjectAndGetID(selectStmt);
	}
	
	/**
	 * Adds a category to the db and returns the primary key of the row added
	 * @param insertStmt insert statement to execute on db 
	 * @return CATEGORY_ID of row inserted
	 */
	private int addObjectAndGetID(String insertStmt) {
		try {
			ArrayList<Integer> insertedIDs = DBUtil.insertAndGetID(insertStmt);
			return insertedIDs.get(0);
		} catch (Exception e) {
			//throw e;
			e.printStackTrace();
		}	
		return -1;
	}
	
	/**
	 * Extracts categories from a ResultSet
	 * @param rs resultSet to extract categories from
	 * @return a list of categories extracted from the resultSet
	 * @throws Exception 
	 */
	public ObservableList<Category> getFromResultSet(ResultSet rs) throws Exception  {
		ObservableList<Category> categoryList = FXCollections.observableArrayList();
		try {
			while (rs.next()) {
				Category category = new Category();
				category.setCategoryID(rs.getInt("CATEGORY_ID"));
				category.setAccountID(rs.getInt("ACCOUNT_ID"));
				category.setName(rs.getString("NAME"));
				category.setIsDeletable(rs.getBoolean("IS_DELETABLE"));
				categoryList.add(category);
			}
		} catch (Exception e) {
			throw e;
		}
		return categoryList;
	}
	
	// TODO: make account_id non-null foreign key later
	public void createTable() {
		String sqlStmt = "create table CATEGORY (" +
				" CATEGORY_ID INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
				" NAME VARCHAR(255)," +
				" IS_DELETABLE BOOLEAN," +
				" ACCOUNT_ID INT CONSTRAINT ACCOUNT_FK" +
				" 	REFERENCES ACCOUNT ON DELETE CASCADE ON UPDATE RESTRICT" +
				")"; 
		try {
			DBUtil.createTable(sqlStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}