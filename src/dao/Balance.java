package dao;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import dao.TransactionDAO.Period;
import util.CalendarPair;
import util.DBUtil;

/**
 * Contains functions to calculate the balance of categories and different periods
 * @author bing
 */
public class Balance {
	private TransactionDAO tDAO;
	
	public Balance() {
		this.tDAO = new TransactionDAO();
	}
	
	/**
	 * Gets spending by each category over a period of time
	 * @param acocuntID ID of the account
	 * @param date Date to calculate spending for
	 * @param period Period to calculate spending
	 * @return A HashMap of categoryID to spending amounts
	 * @throws Exception
	 */
	public HashMap<Integer, Double> spendingByAllCategoryAndDate(int accountID, Calendar date, Period period) throws Exception {
		HashMap<Integer, Double> categorySpendingMap = new HashMap<Integer, Double>();
		CalendarPair dateRange = tDAO.getStartEndDate(date, period);
		Date begin = new Date(dateRange.getStart().getTimeInMillis());
		Date end = new Date(dateRange.getEnd().getTimeInMillis());
		
		String selectStmt = "SELECT CATEGORY_ID, SUM(AMOUNT) AS TOTAL"
				+ " FROM EVENT"
				+ " WHERE DAY >= '"+begin+"'"
				+ " AND   DAY < '"+end+"'"
				+ " AND   ACCOUNT_ID ="+accountID+""
				+ " GROUP BY CATEGORY_ID";
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			while (rs.next()) {
				categorySpendingMap.put(rs.getInt("CATEGORY_ID"), rs.getDouble("TOTAL"));
			}	
		} catch (Exception e) {
			throw new Exception("Database Error (spendingByAllCategoryAndDate)", e);
		}
		return categorySpendingMap;
	}
	
	/**
	 * Calculate the total spending for a category in a given period
	 * @param categoryID ID of the category to search for
	 * @param date Date to search around
	 * @param period DAILY, WEEKLY, MONTHLY or YEARLY
	 * @return Double containing the total spending on the category for the period
	 */
	public double spendingByCategoryAndDate(int categoryID, Calendar date, Period period) throws Exception {
		CalendarPair dateRange = tDAO.getStartEndDate(date, period);
		Date begin = new Date(dateRange.getStart().getTimeInMillis());
		Date end = new Date(dateRange.getEnd().getTimeInMillis());
		double balance = 0;
		
		String selectStmt = "SELECT SUM(AMOUNT) AS TOTAL"
				+ " FROM EVENT, CATEGORY"
				+ " WHERE DAY >= '"+begin+"'"
				+ " AND   DAY < '"+end+"'"
				+ " AND   CATEGORY.CATEGORY_ID = "+categoryID+""
				+ " AND   EVENT.CATEGORY_ID = CATEGORY.CATEGORY_ID";	
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			if (rs.next()) {
				balance = rs.getDouble("TOTAL");
			}
		} catch (Exception e) {
			throw new Exception("Datadase Error (spendingByCategoryAndDate)" ,e);
		}
		return balance;
	}
	
	/**
	 * Calculate the balance for a given date
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date of the day to calculate the balance for
	 * @return The balance of transactions on the date
	 * @throws Exception
	 */
	public double calculateDailyBalance(int accountID, Calendar c) throws Exception {
		try {
			return this.calculatePeriodBalance(accountID, c, Period.DAILY);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Calculate the balance for a given week
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date of the week to calculate the balance for
	 * @return The balance of transactions on the week
	 * @throws Exception
	 */
	public double calculateWeeklyBalance(int accountID, Calendar c) throws Exception {
		try {
			return this.calculatePeriodBalance(accountID, c, Period.WEEKLY);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Calculate the balance for a given month
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date of the month to calculate the balance for
	 * @return The balance of transactions for the month
	 * @throws Exception
	 */
	public double calculateMonthlyBalance(int accountID, Calendar c) throws Exception {
		try {
			return this.calculatePeriodBalance(accountID, c, Period.MONTHLY);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Calculate the balance for a given year
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date containing the year to calculate the balance for
	 * @return The balance of transactions for the year
	 * @throws Exception
	 */
	public double calculateYearlyBalance(int accountID, Calendar c) throws Exception {
		try {
			return this.calculatePeriodBalance(accountID, c, Period.YEARLY);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Calculate the total balance up to the given date
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date containing to calculate the balance up to (inclusive)
	 * @return The balance of transactions for the year
	 * @throws Exception
	 */
	public double calculateTotalBalanceToDate(int accountID, Calendar c) throws Exception {
		Date end = new Date(c.getTimeInMillis());
		String selectStmt = "SELECT IS_EXPENSE, SUM(AMOUNT) AS TOTAL"
			+ " FROM EVENT"
			+ " WHERE DAY <= '"+end+"'"
			+ " AND   ACCOUNT_ID =" + accountID
			+ " GROUP BY IS_EXPENSE";
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			

			double total = 0;
			while (rs.next()) {
				double amount = rs.getDouble("TOTAL");
				total += (rs.getBoolean("IS_EXPENSE")) ? amount * -1 : amount;
			}
			return total;
		} catch (Exception e) {
			throw new Exception("Database Error (calculateTotalBalanceToDate)", e);
		}
	}
	
	/**
	 * Calculate the total balance up to the given date
	 * @param accountID ID of the account to calculate the balance for
	 * @param c Date containing to calculate the balance up to
	 * @return The balance of transactions for the year
	 * @throws Exception
	 */
	private double calculatePeriodBalance(int accountID, Calendar c, Period period) throws Exception {
		CalendarPair calendarPair = tDAO.getStartEndDate(c, period);
		Date start = new Date(calendarPair.getStart().getTimeInMillis());
		Date end = new Date(calendarPair.getEnd().getTimeInMillis());
		
		String selectStmt = "SELECT IS_EXPENSE, SUM(AMOUNT) AS TOTAL"
			+ " FROM  EVENT"
			+ " WHERE DAY >= '"+start+"'"
			+ " AND   DAY < '"+end+"'"
			+ " AND   ACCOUNT_ID =" + accountID
			+ " GROUP BY IS_EXPENSE"
			+ " ORDER BY IS_EXPENSE";

		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			double total = 0;
			while (rs.next()) {
				double amount = rs.getDouble("TOTAL");
				total += (rs.getBoolean("IS_EXPENSE")) ? amount * -1 : amount;
			}
			return total;
		} catch (Exception e) {
			throw new Exception("Database Error (calculatePeriodBalance)", e);
		}
	}
}
