package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dao.TransactionDAO.Period;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Limit;
import util.DBUtil;

public class LimitDAO implements ObjectDAO<Limit>{
	private HashMap<Period, Integer> periodMap;
	private HashMap<Integer, Period> reversePeriodMap;
	
	/**
	 * Initialises maps for processing Limit's Period fields
	 */
	public LimitDAO() {
		this.periodMap = new HashMap<Period, Integer>();
		this.periodMap.put(Period.DAILY, 0);
		this.periodMap.put(Period.WEEKLY, 1);
		this.periodMap.put(Period.MONTHLY, 2);
		this.periodMap.put(Period.YEARLY, 3);
		
		this.reversePeriodMap = new HashMap<Integer, Period>();
		this.reversePeriodMap.put(0, Period.DAILY);
		this.reversePeriodMap.put(1, Period.WEEKLY);
		this.reversePeriodMap.put(2, Period.MONTHLY);
		this.reversePeriodMap.put(3, Period.YEARLY);
	}
	
	/**
	 * Converts a period into an int to store in DB
	 * @param period
	 * @return
	 */
	public int convertPeriod(Period period) {
		return this.periodMap.get(period);
	}
	
	/**
	 * Converts an int to a period to store in a Limit object
	 * @param i
	 * @return
	 */
	public Period getPeriod(int i) {
		return this.reversePeriodMap.get(i);
	}
	
	/**
	 * Gets all limits from the DB
	 */
	public List<Limit> getAllObjects() {
		String selectStmt = "SELECT * FROM LIMIT";
		List<Limit> limitList = new ArrayList<Limit>();
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			limitList = this.getFromResultSet(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return limitList;
	}
	
	/**
	 * Get a limit with a specific limit ID
	 */
	public Limit getObject(int limitID) {
		String selectStmt = "SELECT * FROM LIMIT WHERE LIMIT_ID=" + limitID;
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// Extract fields and put into category object
			List<Limit> limitList = this.getFromResultSet(rs);
			if (limitList.size() > 0) {
				return limitList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get a limit with a specific category ID
	 * @param categoryID The category ID of the limit to get
	 */
	public Limit getObjectWithCategoryID(int categoryID) {
		String selectStmt = "SELECT * FROM LIMIT WHERE CATEGORY_ID=" + categoryID;
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			// Extract fields and put into category object
			List<Limit> limitList = this.getFromResultSet(rs);
			if (limitList.size() > 0) {
				return limitList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Update a limit with a specific ID
	 */
	public void updateObject(Limit Object) {
		String updateStmt = "UPDATE LIMIT" +
				" SET    MAX_AMOUNT = "+Object.getMaxAmount()+"," +
				"        PERIOD = "+this.convertPeriod(Object.getPeriod())+"" +
				" WHERE  LIMIT_ID = "+Object.getLimitID()+"";
		try {
			DBUtil.executeUpdateDB(updateStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete a limit with a specific ID
	 */
	public void deleteObject(Limit Object) {
		String deleteStmt = "DELETE FROM LIMIT"
				+ " WHERE LIMIT_ID = "+Object.getLimitID()+"";
		
		try {
			DBUtil.executeUpdateDB(deleteStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete a limit with a specific categoryID
	 * @param categoryID
	 */
	public void deleteObject(int categoryID) {
		String deleteStmt = "DELETE FROM LIMIT"
				+ " WHERE CATEGORY_ID = " + categoryID;
		
		try {
			DBUtil.executeUpdateDB(deleteStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Inserts a Limit into the Limit table in DB
	 * @param Object Limit to insert
	 */
	public void addObject(Limit Object) {
		String sqlStmt = "INSERT INTO LIMIT" +
				" (MAX_AMOUNT, PERIOD, CATEGORY_ID)" + 
				" VALUES("+Object.getMaxAmount()+", "+this.convertPeriod(Object.getPeriod())+" ,"+Object.getCategoryID()+")";	

		addObject(sqlStmt);
	}
		
	/**
	 * Executes an insert statement
	 * @param sqlStmt Insert statement
	 */
	private void addObject(String sqlStmt) {
		try {
			DBUtil.executeUpdateDB(sqlStmt);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns a list of limits from a resultSet
	 * @param rs ResultSet to get limits from
	 * @return A list of limits from the resultSet
	 * @throws SQLException
	 */
	public ObservableList<Limit> getFromResultSet(ResultSet rs) throws SQLException  {
		ObservableList<Limit> categoryList = FXCollections.observableArrayList();
		while (rs.next()) {
			Limit limit = new Limit();
			limit.setLimitID(rs.getInt("LIMIT_ID"));		
			limit.setCategoryID(rs.getInt("CATEGORY_ID"));
			limit.setMaxAmount(rs.getDouble("MAX_AMOUNT"));
			limit.setPeriod(this.getPeriod(rs.getInt("PERIOD")));
			categoryList.add(limit);
		}
		return categoryList;
	}
		
	/**
	 * Creates a table in the DB with the following fields:
	 * LIMIT_ID INTEGER (PRIMARY KEY)
	 * MAX_AMOUNT DOUBLE
	 * PERIOD INTEGER
	 * CATEGORY_ID INTEGER (FOREIGN KEY)
	 */
	public void createTable() {
		String sqlStmt = "create table LIMIT (" +
				" LIMIT_ID INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
				" MAX_AMOUNT DOUBLE," +
				" PERIOD INTEGER," +
				" CATEGORY_ID INTEGER NOT NULL CONSTRAINT CATEGORY_FK" +
				" 	REFERENCES CATEGORY ON DELETE CASCADE ON UPDATE RESTRICT" +
				")"; 
		try {
			DBUtil.createTable(sqlStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}