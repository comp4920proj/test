package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import util.DBUtil;

public class AccountDAO implements ObjectDAO<Account> {	
	public List<Account> getAllObjects() {
		String selectStmt = "SELECT * FROM ACCOUNT";
		List<Account> accountList = new ArrayList<Account>();
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);
			accountList = this.getFromResultSet(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accountList;
	}
		
	public Account getObject(int id) {
		String selectStmt = "SELECT * FROM ACCOUNT WHERE ACCOUNT_ID=" + id;

		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);

			// Extract fields and put into transaction object
			List<Account> accountList = this.getFromResultSet(rs);
			if (accountList.size() > 0) {
				return accountList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	public Account getObject(String username) {
		String selectStmt = "SELECT * FROM ACCOUNT WHERE"
				+ " USERNAME= '"+username+"'";
		
		try {
			ResultSet rs = DBUtil.executeQueryDB(selectStmt);

			// Extract fields and put into transaction object
			List<Account> accountList = this.getFromResultSet(rs);
			if (accountList.size() > 0) {
				return accountList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;	
	}
	
	public List<Account> getFromResultSet(ResultSet rs) throws SQLException  {
		List<Account> accountList = new ArrayList<Account>();
		while (rs.next()) {
			Account account = new Account();
			account.setAccountID(rs.getInt("ACCOUNT_ID"));
			account.setEmail(rs.getString("EMAIL"));
			account.setUsername(rs.getString("USERNAME"));
			account.setPassword(rs.getString("PASSWORD"));
			accountList.add(account);
		}

		return accountList;
	}
	
	public void updateObject(Account o) {
		String updateStmt = "UPDATE ACCOUNT" +
				" SET    EMAIL = '"+o.getEmail()+"'," + 
				"		 USERNAME = '"+o.getUsername()+"'," +
				"        PASSWORD = '"+o.getPassword()+"'" +
				" WHERE  ACCOUNT_ID = "+o.getAccountID()+"";

		try {
			DBUtil.executeUpdateDB(updateStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteObject(Account o) {
		String deleteStmt = "DELETE FROM ACCOUNT "
				+ " WHERE ACCOUNT_ID = "+o.getAccountID()+"";
		
		try {
			DBUtil.executeUpdateDB(deleteStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Use addObjectAndGetID
	 * @param o
	 */
	@Deprecated
	public void addObject(Account o) {
		String sqlStmt = "INSERT INTO ACCOUNT" +
				" (EMAIL, USERNAME, PASSWORD)" + 
				" VALUES('"+o.getEmail()+"', '"+o.getUsername()+"', '"+o.getPassword()+"'" + ")";
		
		try {
			DBUtil.executeUpdateDB(sqlStmt);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int addObjectAndGetID(Account o) {
		String sqlStmt = "INSERT INTO ACCOUNT" +
				" (EMAIL, USERNAME, PASSWORD)" + 
				" VALUES('"+o.getEmail()+"', '"+o.getUsername()+"', '"+o.getPassword()+"'" + ")";
		
		try {			
			ArrayList<Integer> insertedIDs = DBUtil.insertAndGetID(sqlStmt);
			return insertedIDs.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return -1;
	}
	
	public void createTable() {
		String sqlStmt = "create table ACCOUNT (" +
				" ACCOUNT_ID INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
				" EMAIL LONG VARCHAR," +
				" USERNAME VARCHAR(64) UNIQUE," +
				" PASSWORD LONG VARCHAR" +
				")"; 
		
		try {
			DBUtil.createTable(sqlStmt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}